{% set author = "Kaukassus" %}
{% set required = "1.50" %}
{% set huntir = false %}
{% set riflegrenades = false %}
{% set flashlights = false %}

{#/*

This is a Comment.
This can be as long as I want and it won't show up in the final version of the file!
Allowing me to not have to use any kind of minimizer to remove big chunks of comments!

*/#}
{% 
  set null_eval = eval("
  console.log('Oh Sweet, I can run JavaScript Code!');
  console.log('Second Statement!');
  var nul = '9000';
  console.log('Look ma, concats! ' + nul);
  ")
%}
{{nul}}

class CfgPatches {
  class kauk_weapon_patches {
    units[] = {};
    weapons[] = {};
    author[] = {"{{author}}"};
    requiredVersion = {{required}};
    requiredAddons[] = {"r3f_armes_c", "asdg_jointrails", "A3_Weapons_F","ace_huntir"};
  };
};

class Mode_SemiAuto;
class Mode_FullAuto;

class CfgWeapons {
  class UGL_F;
  class Rifle_Base_F;
  class ItemCore;

  //MX Configuration
  class arifle_MX_Base_F : Rifle_Base_F {
    class Single : Mode_SemiAuto {
      class SilencedSound {
        begin1[] = {"kauk_weapon_patches\sounds\MX\m4sd_fire",0.794328,1,400};
        soundBegin[] = {"begin1",1};
      };
      class StandardSound {
        begin1[] = {"kauk_weapon_patches\sounds\MX\gpmg_fire_1p.wss",3.16228,1,1800};
        soundBegin[] = {"begin1",1};
      };
    };
    class FullAuto : Mode_FullAuto {
      class SilencedSound {
        begin1[] = {"kauk_weapon_patches\sounds\MX\m4sd_fire",0.794328,1,400};
        soundBegin[] = {"begin1",1};
      };
      class StandardSound {
        begin1[] = {"kauk_weapon_patches\sounds\MX\gpmg_fire_1p.wss",3.16228,1,1800};
        soundBegin[] = {"begin1",1};
      };
    };
  };
  class arifle_MX_SW_F : arifle_MX_Base_F {
    class manual : FullAuto {
      class SilencedSound {
        begin1[] = {"kauk_weapon_patches\sounds\MX\m4sd_fire",0.794328,1,400};
        soundBegin[] = {"begin1",1};
      };
      class StandardSound {
        begin1[] = {"kauk_weapon_patches\sounds\MX\gpmg_fire_1p.wss",3.16228,1,1800};
        soundBegin[] = {"begin1",1};
      };
    };
  };

  //F2000 Configuration
  class mk20_base_F : Rifle_Base_F {
    class Single : Mode_SemiAuto {
      class StandardSound {
        begin1[] = {"kauk_weapon_patches\sounds\F2000\hk416_fire_1p.wss",2.51189,1,1400};
        soundBegin[] = {"begin1",1};
      };
    };
    class FullAuto : Mode_FullAuto {
      class StandardSound {
        begin1[] = {"kauk_weapon_patches\sounds\F2000\hk416_fire_1p.wss",2.51189,1,1400};
        soundBegin[] = {"begin1",1};
      };
    };
  };

  //TAR21
  class Tavor_base_F : Rifle_Base_F {
    class Single : Mode_SemiAuto {
      class StandardSound {
        begin1[] = {"kauk_weapon_patches\sounds\Tar\tar21_fire_1p.wss",1.99526,1,1400};
        soundBegin[] = {"begin1",1};
      };
    };
    class FullAuto : Mode_FullAuto {
      class StandardSound {
        begin1[] = {"kauk_weapon_patches\sounds\Tar\tar21_fire_1p.wss",1.99526,1,1400};
        soundBegin[] = {"begin1",1};
      };
    };
  };

 class CannonCore;
  class Item_Base_F;
  class Item_Laserdesignator : Item_Base_F {
    visionMode[] = {"Normal","NVG", "Ti"};    //Add Ti Option to Laserdesignator Again. I like the option of having Ti on it.
  };
  
  {% if huntir %}{% include "./HuntIR/huntir.hpp" %}{% endif %}
  {% if flashlights %}{% include "./flashlights.hpp" %}{% endif %}
};
{% if riflegrenades %}{% include "./riflegrenades/r3f_riflegrenades.hpp" %}{% endif %}
#![feature(fs_walk)]

use std::fs;
use std::env;
use std::path::Path;
use std::path::PathBuf;

use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::fs::OpenOptions;

use std::process::Command;

const SPACEAMOUNT: &'static str = "  ";


#[allow(unused_must_use)]
fn main() {
    let cwd = env::current_dir().unwrap();
    let cwd_str: &str = cwd.as_path().to_str().unwrap();

    println!("Current Dir: {}", cwd_str);

    let path = Path::new(".");
    let dirs: fs::WalkDir = fs::walk_dir(&path).unwrap();

    for d in dirs {
      let direntry: fs::DirEntry = d.unwrap();
      let pathbuf: PathBuf = direntry.path();
      let path: &Path = pathbuf.as_path();
      let meta: fs::Metadata = direntry.metadata().unwrap();

      if meta.is_file() {

        //Check the File extension of the file:
        let extension: &str = match path.extension() {
            Some(c) => c.to_str().unwrap(),
            None => "None"
        };

        let shouldformat: bool = match extension {
          "sqf" => true,
          "hpp" => true,
          "cpp" => true,
          "ext" => true,
          "paa" => false,
          "wss" => false,
          _ => false,
        };

        if shouldformat {
          println!("Found File: {:?}", path);
          println!("Converting Tabs to Spaces..");
          {
            let mut file = match File::open(&path) {
              Err(why) => panic!("couldn't open {}: {}", path.display(), Error::description(&why)),
              Ok(file) => file,
            };
            let mut file_contents = String::new();
            file.read_to_string(&mut file_contents);

            //Converting Tabs to Spaces
            file_contents = file_contents.replace("\t", SPACEAMOUNT);
            let mut file = match OpenOptions::new()
              .read(true)
              .write(true)
              .create(true)
              .open(&path) {
                Err(why) => panic!("couldn't open {}: {}", path.display(), Error::description(&why)),
                Ok(file) => file,
            };
            file.write_all(file_contents.as_bytes());
          }

          //Convert CRLF to UNIX Line Endings by utilyzing the dos2unix command
          println!("Converting DOS to UNIX Line endings..");

          Command::new("dos2unix.exe")
            .arg(path.to_str().unwrap())
            .output().unwrap_or_else(|e| {
              panic!("failed to execute process: {}", e)
          });

        }
      }
    }
}

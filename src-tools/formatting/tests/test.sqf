[] spawn {
  sleep 5;

  ["KaukMod", "kauk_flittgt", "Aircraft Mark Target", {[] call kauk_fnc_laserTarget}, {false}, [44, [true, false, false]],false] call CBA_fnc_addKeybind;

  player setVariable ["Kauk_TGT_On", false];
  kauk_TGT_laserObj = objNull;

  kauk_fnc_laserTarget = {
  if (vehicle player isKindOf "Air") then {
    _state = player getVariable "Kauk_TGT_On";
    if (_state) then {
    deleteVehicle kauk_TGT_laserObj;
    kauk_TGT_laserObj = objNull;
    hint "TGT OFF";
    player setVariable ["Kauk_TGT_On", false];
    } else {
    kauk_TGT_laserObj = "Kauk_LaserDesignation" createVehicle (atltoasl screentoworld [0.5,0.5]);
    hint "TGT ON";
    player setVariable ["Kauk_TGT_On", true];
    };
  };
  };
};

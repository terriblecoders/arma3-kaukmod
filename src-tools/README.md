#Information

This folder contains native applications that are not addons for Arma, but are used in the development of these addons.

Most of these Programs will use Rust as their main programming Language. Rust is a new Kind of System programming language that aims to have a High-Level Syntax with Low-Level access and functionality.

Why a compiled language instead of an interpreted language you may ask.
This simply personal taste and used as a learning experience for the Rust programming language.

The advantage of rust is that it produces a native binary and Every major operating system is supported. It can also easily incorporate C and C++ code and hook into C and C++ libraries which is incredibly useful if needed.

##Building
Since these programs are written in Rust, you need the latest nightly version of the Rust Compiler and the Cargo package manager.

The nightly version is needed to make use of unstable features, which are not allowed in stable or beta releases of Rust.

Both of these are in the Main installer and can be downloaded from this location: https://www.rust-lang.org/install.html

After installing Rust, you can build the applications by goin into their Directory where the Cargo.toml resides and execute the following command:

```
cargo build --release
```

After that, you can find your binary in the target/release folder.

The binaries compiled contain native code that corresponds with the architecture of the operating system installed. That means If you are running a x86_64 operating system, the binary produced will be 64bit and will only work on those.

##Applications
The Following Programs are Listed in this folder:

###Make
Make is the main Build Script of this Repository, which is used to build all scripts by the push of a button.

It is not mandatory and just serves as a way to compile all kauk_* addon folders into a pbo with a simple execution of the make.exe binary.

This Application requires the PBOConsole in the users %PATH% Variable for it to work.

PBOConsole can be downloaded and installed from this link: http://www.armaholic.com/page.php?id=16369

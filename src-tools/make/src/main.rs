//use std::process::Command;
use std::fs;
use std::process::Command;
use std::env;

fn main() {
    println!("Building Files...");

    let cwd = env::current_dir().unwrap();
    let cwd_str: &str = cwd.as_path().to_str().unwrap();

    println!("Current Dir: {}", cwd_str);

    match fs::read_dir(".") {
      Err(why) => println!("! {:?}", why.kind()),
      Ok(paths) => for path in paths {
        let path_a = path.unwrap().path();
        let name: &str = path_a.as_path().to_str().unwrap();
        let metadata = fs::metadata(name).unwrap();

        if metadata.is_dir() {
          //It's a Folder
          let v: Vec<&str> = name.split("kauk").collect();
          if v.len() == 2 {
            //Check for Kauk Prefix

            let s: Vec<&str> = name.split(".").collect();
            let source_path: String = format!("{}{}", cwd_str, s[1]);
            let target_path: String = format!("{}.pbo", source_path);
            println!("Compiling Addon: {}", s[1]);

            let output = Command::new("PBOConsole")
              .arg("-pack")
              .arg(source_path)
              .arg(target_path)
              .output().unwrap_or_else(|e| {
                panic!("failed to execute process: {}", e)
            });

          }
        }
      },
    }
}
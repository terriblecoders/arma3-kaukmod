'use strict';

var fs = require('fs'),
  path = require('path');

var gulp = require('gulp');
var swig = require('gulp-swig');
var data = require('gulp-data');
var runSequence = require('run-sequence');

global.quote = '\"';
global.squote = '\'';

//Main Documentation to get started with swig
//
//https://paularmstrong.github.io/swig/docs/
//https://paularmstrong.github.io/swig/docs/tags/

function log(message) {
  var pre = '\n# Running: ';
  var delimiter = '#-';
  var amount = message.length + 10;
  for (var i = 0; i < amount; i++) {
    delimiter = delimiter + '-';
  }
  delimiter = delimiter + '#';
  console.log(delimiter + pre + message + ' #\n' + delimiter + '\n');
}

gulp.task('copy', function(cb) {
  log('Copying Project files to deployment')
  var srcpath = './src';
  var dirs = fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
  for (var i = 0; i < dirs.length; i++) {
    var destination = './dist/deployment/' + dirs[i];
    gulp.src(srcpath + '/' + dirs[i] + '/**/*')
      .pipe(gulp.dest(destination));
  }
  cb();
});

var swig_data = function(file) {
  return require('./swig_data.json');
};

var opts_cpp = {ext : ".cpp"};
var opts_sqf = {ext : ".sqf"};

//----------------------------------#
//CPP Template Generation

gulp.task('swig-cpp', function() {
  log('Building .cpp Files');
  var srcpath = './src';
  var dirs = fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
  for (var i = 0; i < dirs.length; i++) {
    var destination = './dist/deployment_swig/' + dirs[i];
    gulp.src(srcpath + '/' + dirs[i] + '/**/config.cpp')
      .pipe(data(swig_data))
      .pipe(swig(opts_cpp))
      .pipe(gulp.dest(destination));
  }
});

gulp.task('swig-sqf', function() {
  log('Building .sqf Files');
  var srcpath = './src';
  var dirs = fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
  for (var i = 0; i < dirs.length; i++) {
    var destination = './dist/deployment_swig/' + dirs[i];
    gulp.src(srcpath + '/' + dirs[i] + '/**/*.sqf')
      .pipe(data(swig_data))
      .pipe(swig(opts_sqf))
      .pipe(gulp.dest(destination));
  }
});

//----------------------------------#
//CPP Template Generation

gulp.task('prepare-deployment', function(callback) {
  runSequence(
    'copy',
    ['swig-cpp', 'swig-sqf'],
    callback
  );
});
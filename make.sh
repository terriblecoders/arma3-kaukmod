#!/bin/bash

#Fast way to clean Environment without using Gulp
rm -rf dist/deployment*

gulp prepare-deployment

cp -R dist/deployment_swig/* dist/deployment

#Remove stuff we don't need
rm -rf dist/deployment_swig
rm -rf dist/deployment/*/includes
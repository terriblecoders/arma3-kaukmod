{%- set i_addActionColor = "#c4db93" -%}

Kauk_Logistics_obj_fall = {
  _obj    = _this select 0;
  _height = (getPos _obj) select 2;
  _fall   = 0.09;
  while {((getPos _obj) select 2) > 0.1} do
  {
    _fall = (_fall * 1.1);
    _obj setPos [getPos _obj select 0, getPos _obj select 1, _height];
    _height = _height - _fall;
    sleep 0.01;
  };
};

{% if logistics.enable_rotation_function -%}
[] spawn {
  sleep 5;
  ["KaukMod", "kauk_logi_dragleft", "Drag Rotate", {[] call kauklog_fnc_DragRotate}, {false}, [16, [false, false, false]],false] call CBA_fnc_addKeybind;
};
{%- endif %}

[] spawn {
  waitUntil {!isNull player};

  if (isServer) then {
    Kauk_log_id_repo = 10;publicVariable "Kauk_log_id_repo";
    Kauk_log_cargo_repo = "Land_HBarrierBig_F" createVehicle [- 5000,- 5000,0];publicVariable "Kauk_log_cargo_repo";
  };

  Kauk_log_lift_min_h    = 1;
  Kauk_log_lift_max_h    = 50;
  Kauk_log_lift_radius   = 4;
  Kauk_log_liftobjects = [ //Things you can Lift
    "AllVehicles", "HBarrier_base_F", "Strategic", "Slingload_01_Base_F"
  ];
  Kauk_log_loadobjects = [ //Things you can Load
    "All"
  ];

  Kauk_log_loaderobjects = [ // Things that can Load
    "All"
  ];

  Kauk_log_dragObjects = [ //Things you can Drag
    "All"
  ];
  Kauk_log_dragObjectsBlackList = [ //Stop Atlas LHD Objects from being detected
    "ATLAS_LHD_1",
    "ATLAS_LHD_2",
    "ATLAS_LHD_3",
    "ATLAS_LHD_4",
    "ATLAS_LHD_5",
    "ATLAS_LHD_5a",
    "ATLAS_LHD_6",
    "ATLAS_LHD_7",
    "ATLAS_LHD_elev_1",
    "ATLAS_LHD_elev_2",
    "ATLAS_LHD_house_1",
    "ATLAS_LHD_house_2",
    "ATLAS_LHD_Int_1",
    "ATLAS_LHD_Int_2",
    "ATLAS_LHD_Int_3"
  ];

  kauk_log_canLoad = [     //Objects you can load stuff into
    "Land",  //Land Vehicles
    "Air",  //Air Vehicles
    "Ship",  //Ships and Submarines
    "Slingload_01_Base_F", //Huron Slingload containers
    "Pod_Heli_Transport_04_base_F" //Taru Helicopter Containers
  ];
  Kauk_log_cargo_selected = ObjNull;
  Kauk_log_action_cargo = false;
  Kauk_Logistics_isLifted = 0;
  Kauk_Logistics_isLifted_Modern = 0;
  kauklog_Drag_isDragging = false;

  //Check Loop
  [] spawn {
    while {true} do {
      sleep 1;
      Kauk_Logistics_showLiftAction = [] call kauklog_fnc_SlingCheck;
      Kauk_Logistics_showDragAction = [] call kauklog_fnc_DragCheck;
      Kauk_Logistics_showLoadAction = [] call kauklog_fnc_LoadCheck;
    };
  };

  player addAction ["<t color=""{{i_addActionColor}}"">Lift</t>",{call kauklog_fnc_SlingObject;}, [], 9, false, false, "", "Kauk_Logistics_showLiftAction"];
  player addAction ["<t color=""{{i_addActionColor}}"">Lift (old)</t>",{call kauklog_fnc_SlingObjectLegacy;}, [], 8, false, false, "", "Kauk_Logistics_showLiftAction"];
  player addAction ["<t color=""{{i_addActionColor}}"">Release</t>",{call kauklog_fnc_ReleaseObject;}, [], 9, false, false, "", "Kauk_Logistics_isLifted == 1"];

  player addaction ["<t color=""{{i_addActionColor}}"">Select</t>",{call kauklog_fnc_SelectObject;},[],-7,false,false, "", "Kauk_Logistics_showLoadAction"];
  player addaction ["<t color=""{{i_addActionColor}}"">Check Content</t>",{call kauklog_fnc_CheckContent;},[],-7,false,false, "", "Kauk_Logistics_showLoadAction"];
  player addaction ["<t color=""{{i_addActionColor}}"">Load</t>",{call kauklog_fnc_LoadObject;},[],-7,false,false, "", "Kauk_Logistics_showLoadAction"];

  player addaction ["<t color=""{{i_addActionColor}}"">Drag</t>",{call kauklog_fnc_DragObject;},[],-7,false,false, "", "Kauk_Logistics_showDragAction"];
  player addaction ["<t color=""{{i_addActionColor}}"">Release Drag</t>",{call kauklog_fnc_DragRelease;},[],-7,false,false, "", "kauklog_Drag_isDragging"];

  player addAction ["<t color=""{{i_addActionColor}}"">Attach Object</t>",{call kauklog_fnc_AttachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];
  player addAction ["<t color=""{{i_addActionColor}}"">Stop Attach Object</t>",{[true] call kauklog_fnc_StopAttachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];
  player addAction ["<t color=""{{i_addActionColor}}"">Abort Attach Object</t>",{[false] call kauklog_fnc_StopAttachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];

  player addAction ["<t color=""{{i_addActionColor}}"">Detach Object</t>",{call kauklog_fnc_DetachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];
  player addAction ["<t color=""{{i_addActionColor}}"">Align Object</t>",{call kauklog_fnc_AlignObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];

  player addEventHandler ["Respawn", {
    [] spawn {
      WaitUntil {sleep 1; Alive player};
      Kauk_Logistics_CargoLifted = ObjNull;
      Kauk_log_action_cargo = false;
      Kauk_Logistics_isLifted = 0;
      Kauk_Logistics_isLifted_Modern = 0;
      kauklog_Drag_isDragging = false;

      player addAction ["<t color=""{{i_addActionColor}}"">Lift</t>",{call kauklog_fnc_SlingObject;}, [], 9, false, false, "", "Kauk_Logistics_showLiftAction"];
      player addAction ["<t color=""{{i_addActionColor}}"">Lift (old)</t>",{call kauklog_fnc_SlingObjectLegacy;}, [], 8, false, false, "", "Kauk_Logistics_showLiftAction"];
      player addAction ["<t color=""{{i_addActionColor}}"">Release</t>",{call kauklog_fnc_ReleaseObject;}, [], 9, false, false, "", "Kauk_Logistics_isLifted == 1"];

      player addaction ["<t color=""{{i_addActionColor}}"">Select</t>",{call kauklog_fnc_SelectObject;},[],-7,false,false, "", "Kauk_Logistics_showLoadAction"];
      player addaction ["<t color=""{{i_addActionColor}}"">Check Content</t>",{call kauklog_fnc_CheckContent;},[],-7,false,false, "", "Kauk_Logistics_showLoadAction"];
      player addaction ["<t color=""{{i_addActionColor}}"">Load</t>",{call kauklog_fnc_LoadObject;},[],-7,false,false, "", "Kauk_Logistics_showLoadAction"];

      player addaction ["<t color=""{{i_addActionColor}}"">Drag</t>",{call kauklog_fnc_DragObject;},[],-7,false,false, "", "Kauk_Logistics_showDragAction"];
      player addaction ["<t color=""{{i_addActionColor}}"">Release Drag</t>",{call kauklog_fnc_DragRelease;},[],-7,false,false, "", "kauklog_Drag_isDragging"];

      player addAction ["<t color=""{{i_addActionColor}}"">Attach Object</t>",{call kauklog_fnc_AttachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];
      player addAction ["<t color=""{{i_addActionColor}}"">Stop Attach Object</t>",{[true] call kauklog_fnc_StopAttachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];
      player addAction ["<t color=""{{i_addActionColor}}"">Abort Attach Object</t>",{[false] call kauklog_fnc_StopAttachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];

      player addAction ["<t color=""{{i_addActionColor}}"">Detach Object</t>",{call kauklog_fnc_DetachObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];
      player addAction ["<t color=""{{i_addActionColor}}"">Align Object</t>",{call kauklog_fnc_AlignObject;}, [], 9, false, false, "", "Kauk_Logistics_showDragAction"];

    };
  }];
};
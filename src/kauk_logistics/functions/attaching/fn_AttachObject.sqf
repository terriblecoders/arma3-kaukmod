_ins = lineIntersectsSurfaces [
  AGLToASL positionCameraToWorld [0,0,0],
  AGLToASL positionCameraToWorld [0,0,10],
  player,
  objNull,
  true,
  1,
  "GEOM",
  "NONE"
];
_obj = (_ins select 0 select 2);
if (!(isNil {_obj})) then {

  Kauk_log_attach_pointer_object = "Sign_Arrow_F" createVehicle [0,0,0];
  Kauk_log_attach_source_object = _obj;

  ["Kauk_log_attach_handler", "onEachFrame", {
    _ins = lineIntersectsSurfaces [
      AGLToASL positionCameraToWorld [0,0,0],
      AGLToASL positionCameraToWorld [0,0,10],
      player,
      objNull,
      true,
      1,
      "GEOM",
      "NONE"
    ];
    if (count _ins > 0) then {
      Kauk_log_attach_target_object = (_ins select 0 select 2);
      Kauk_log_attach_vector = (_ins select 0 select 1);
      Kauk_log_attach_pos = (_ins select 0 select 0);
      Kauk_log_attach_pointer_object setPosASL Kauk_log_attach_pos;
      Kauk_log_attach_pointer_object setVectorUp Kauk_log_attach_vector;
    };
  }] call BIS_fnc_addStackedEventHandler;
};
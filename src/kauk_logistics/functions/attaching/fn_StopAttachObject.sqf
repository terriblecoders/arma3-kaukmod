["Kauk_log_attach_handler", "onEachFrame"] call BIS_fnc_removeStackedEventHandler;
deleteVehicle Kauk_log_attach_pointer_object;
if (_this select 0) then {
  Kauk_log_attach_source_object setPosASL Kauk_log_attach_pos;
  Kauk_log_attach_source_object attachTo [Kauk_log_attach_target_object];
  Kauk_log_attach_source_object setVectorUp Kauk_log_attach_vector;
}
_ins = lineIntersectsSurfaces [
  AGLToASL positionCameraToWorld [0,0,0],
  AGLToASL positionCameraToWorld [0,0,10],
  player,
  objNull,
  true,
  1,
  "GEOM",
  "NONE"
];
_obj = (_ins select 0 select 2);
if (!(isNil {_obj})) then {
  { detach _x; } forEach attachedObjects _obj;
};
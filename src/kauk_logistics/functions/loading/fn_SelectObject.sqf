_ins = lineIntersectsSurfaces [
  AGLToASL positionCameraToWorld [0,0,0],
  AGLToASL positionCameraToWorld [0,0,10],
  player,
  objNull,
  true,
  1,
  "GEOM",
  "NONE"
];
_veh = (_ins select 0 select 2);
if (!(isNil {_veh})) then {
  Kauk_log_cargo_selected = _veh;
  hint parseText format ["%1 selected",getText (configFile >> "cfgVehicles" >> typeof _veh >> "displayName")];
};
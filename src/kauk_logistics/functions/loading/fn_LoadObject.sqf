private ["_veh_name","_obj_name"];
_cargo_cont = [];
_ins = lineIntersectsSurfaces [
  AGLToASL positionCameraToWorld [0,0,0],
  AGLToASL positionCameraToWorld [0,0,10],
  player,
  objNull,
  true,
  1,
  "GEOM",
  "NONE"
];
_veh = (_ins select 0 select 2);
if (!(isNil {_veh})) then {
  _veh_name = getText (configFile >> "cfgVehicles" >> typeof _veh >> "displayName");
  _obj_name = getText (configFile >> "cfgVehicles" >> typeof Kauk_log_cargo_selected >> "displayName");
  if (Kauk_log_cargo_selected distance _veh < 15 && speed _veh < 2) then
  {
    hint format ["Loading %1 in %2",_obj_name,_veh_name];
    if (Alive Kauk_log_cargo_selected && Alive _veh && Kauk_log_cargo_selected distance _veh < 15 && speed _veh <= 2 && speed _veh >= -2) then {
      if (isNil {_veh getVariable "Kauk_log_cargo_cont"}) then {_veh setVariable ["Kauk_log_cargo_cont",[],false];};
      _cargo_cont = _veh getVariable "Kauk_log_cargo_cont";
      _cargo_cont = _cargo_cont + [Kauk_log_cargo_selected];
      _veh setVariable ["Kauk_log_cargo_cont",_cargo_cont,true];
      Kauk_log_cargo_selected attachTo [Kauk_log_cargo_repo,[0,0,Kauk_log_id_repo]];
      Kauk_log_id_repo = Kauk_log_id_repo + 15;publicVariable "Kauk_log_id_repo";
      Kauk_log_cargo_selected = objNull;
      hint format ["%1 has been loaded in %2",_obj_name,_veh_name];
    } else {hint format ["%1 has not been loaded",_obj_name];};
  };
};
_veh = Kauk_log_temp_veh;
_obj = Kauk_log_temp_obj;
Kauk_log_action_cargo = true;
_cargo_cont = _veh getVariable "Kauk_log_cargo_cont";
_id = _cargo_cont find _obj;
if (_id != -1) then
{
  _veh_name = getText (configFile >> "cfgVehicles" >> typeof _veh >> "displayName");
  _obj_name = getText (configFile >> "cfgVehicles" >> typeof _obj >> "displayName");
  hint format ["Unloading %1 from %2",_obj_name,_veh_name];
  sleep 3;
  hint format ["%1 has been unloaded",_obj_name,_veh_name];
  _cargo_cont set [_id,0];
  _cargo_cont = _cargo_cont - [0];
  _veh setVariable ["Kauk_log_cargo_cont",_cargo_cont,true];
  _height = getPos (vehicle player) select 2;
  deTach _obj;
  _obj setVelocity [0,0,0];
  _obj setpos (_veh modelToWorld [-3,0,0]);
  switch (true) do
  {
    /*case (_height >= 20):
    {
      _obj_para = [_veh,_obj,"B_Parachute_02_F"] spawn BTC_l_paradrop;
    };*/
    case ((_height < 20) && (_height >= 2)):
    {
      _obj setPos [getpos _veh select 0,getpos _veh select 1,(getpos _veh select 2) -1];
      sleep 0.1;
      if (_obj isKindOf "Strategic") then {_obj_fall = [_obj] spawn Kauk_Logistics_obj_fall;};
    };
    case (_height < 2):
    {
      _obj setpos (_veh modelToWorld [-5,0,0]);
    };
  };
};
sleep 1;
Kauk_log_action_cargo = false;
if (!(vehicle player isKindOf "Man")) exitWith {false};
_checkStatus = false;
_ins = lineIntersectsSurfaces [
  AGLToASL positionCameraToWorld [0,0,0],
  AGLToASL positionCameraToWorld [0,0,10],
  player,
  objNull,
  true,
  1,
  "GEOM",
  "NONE"
];
_object = (_ins select 0 select 2);
if (!(isNil {_object})) then {
  { if (_object isKindOf _x) then { _checkStatus = true;}; } count Kauk_log_loaderobjects;
};
_checkStatus
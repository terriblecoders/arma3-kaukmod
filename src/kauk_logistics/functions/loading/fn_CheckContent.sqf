[] spawn {
  _ins = lineIntersectsSurfaces [
    AGLToASL positionCameraToWorld [0,0,0],
    AGLToASL positionCameraToWorld [0,0,10],
    player,
    objNull,
    true,
    1,
    "GEOM",
    "NONE"
  ];
  _veh = (_ins select 0 select 2);
  if (!(isNil {_veh})) then {
    if (isNil {_veh getVariable "Kauk_log_cargo_cont"}) then {_veh setVariable ["Kauk_log_cargo_cont",[],false];};
    private ["_cargo"];
    _text = "";
    _cargo = _veh getVariable "Kauk_log_cargo_cont";
    _text = _text + "Vehicle: " + getText (configFile >> "cfgVehicles" >> typeof _veh >> "displayName") + "<br/>Cargo:<br/>";
    if (count _cargo > 0) then
    {
      {
        _text = _text + getText (configFile >> "cfgVehicles" >> typeof _x >> "displayName") + "<br/>";
        [_x,_veh] spawn
        {
          _obj = _this select 0;
          _veh = _this select 1;

          Kauk_log_temp_obj = _obj;
          Kauk_log_temp_veh = _veh;

          private ["_sleep"];
          _unload = _veh addaction ["<t color=""#baed27"">" + (format ["UnLoad %1",getText (configFile >> "cfgVehicles" >> typeof _obj >> "displayName")]) + "</t>",{call kauklog_fnc_UnloadContent;},[],-7,false,false];
          _sleep = time + 15;
          waitUntil {Kauk_log_action_cargo || (time > _sleep)};
          _veh removeAction _unload;
        };
      } forEach _cargo;
      hint parseText _text;
    } else {_text = _text + "Empty";hint parseText _text;};
  };
};
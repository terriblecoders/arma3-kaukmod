_cargo = objNull;
_aircraft = vehicle player;
_cargo_array = nearestObjects [_aircraft, Kauk_log_liftobjects, 50];
if (count _cargo_array > 0 && driver (_cargo_array select 0) == player) then {_cargo_array set [0,0];_cargo_array = _cargo_array - [0];};
if (count _cargo_array > 0) then {
  _continue = true;
  {
    if (_continue) then {
      if (!(_x isKindOf "Man") && !(_x == vehicle player)) then {
        _continue = false;
        _cargo = _x;
      };
    };
  } count _cargo_array;
};
if (isNull _cargo) exitWith {};

Kauk_Logistics_isLifted    = 1;
_cargo_pos    = getPosATL _cargo;
_rel_pos      = _aircraft worldToModel _cargo_pos;
_height       = (_rel_pos select 2) + 2.5;
_cargo attachTo [_aircraft, [0,0,_height]];
_name_cargo  = getText (configFile >> "cfgVehicles" >> typeof _cargo >> "displayName");
_aircraft vehicleChat format ["%1 lifted", _name_cargo];
Kauk_Logistics_CargoLifted = _cargo;
if (!(vehicle player isKindOf "Air") || Kauk_Logistics_isLifted == 1) exitWith {false};
_aircraft  = vehicle player;
_can_lift = false;
_cargo_obj = objNull;

_cargo_array = nearestObjects [_aircraft, Kauk_log_liftobjects, 50];
if (count _cargo_array > 0 && driver (_cargo_array select 0) == player) then {_cargo_array set [0,0];_cargo_array = _cargo_array - [0];};
if (count _cargo_array > 0) then {
  _continue = true;
  {
    if (_continue) then {
      if (!(_x isKindOf "Man") && !(_x == vehicle player)) then {
        _continue = false;
        _cargo_obj = _x;
      };
    };
  } count _cargo_array;
};

if (!isNull _cargo_obj) then
{
  _cargo_pos = getPosATL _cargo_obj;
  _rel_pos   = _aircraft worldToModel _cargo_pos;
  Kauk_cargo_obj_x   = _rel_pos select 0;
  Kauk_cargo_obj_y   = _rel_pos select 1;
  Kauk_cargo_obj_z   = _rel_pos select 2;

  if (((abs Kauk_cargo_obj_z) < Kauk_log_lift_max_h) && ((abs Kauk_cargo_obj_z) > Kauk_log_lift_min_h) && ((abs Kauk_cargo_obj_x) < Kauk_log_lift_radius) && ((abs Kauk_cargo_obj_y) < Kauk_log_lift_radius)) then
  {_can_lift = true;} else {_can_lift = false;};
};
_can_lift
_cargo = objNull;
_aircraft = vehicle player;
_cargo_array = nearestObjects [_aircraft, Kauk_log_liftobjects, 50];
if (count _cargo_array > 0 && driver (_cargo_array select 0) == player) then {_cargo_array set [0,0];_cargo_array = _cargo_array - [0];};
if (count _cargo_array > 0) then {
  _continue = true;
  {
    if (_continue) then {
      if (!(_x isKindOf "Man") && !(_x == vehicle player)) then {
        _continue = false;
        _cargo = _x;
      };
    };
  } count _cargo_array;
};
if (isNull _cargo) exitWith {};

Kauk_Logistics_isLifted    = 1;
Kauk_Logistics_isLifted_Modern = 1;
_cargo_pos    = getPosATL _cargo;
_aircraft_pos = getPosATL _aircraft;
_rel_pos      = _aircraft worldToModel _cargo_pos;

Kauk_Logistics_Carrier = createVehicle ["Box_IND_Wps_F", [_aircraft_pos select 0, _aircraft_pos select 1, _rel_pos select 2], [], 0, "CAN_COLLIDE"];
Kauk_Logistics_Carrier setDir (getDir _aircraft);
Kauk_Logistics_Carrier setMass 300;
Kauk_Logistics_Carrier allowDamage false;
Kauk_Logistics_Carrier lockCargo true;

Kauk_Logistics_Carrier setObjectTexture [0, "\kauk_logistics\img\invinsible.paa"];
Kauk_Logistics_Carrier setObjectTexture [1, "\kauk_logistics\img\invinsible.paa"];

Kauk_Logistics_Carrier_rope1 = ropeCreate [_aircraft, [0,0,0], Kauk_Logistics_Carrier, [1.5,1.5,0]];
Kauk_Logistics_Carrier_rope2 = ropeCreate [_aircraft, [0,0,0], Kauk_Logistics_Carrier, [1.5,-1.5,0]]; //asd
Kauk_Logistics_Carrier_rope3 = ropeCreate [_aircraft, [0,0,0], Kauk_Logistics_Carrier, [-1.5,1.5,0]];
Kauk_Logistics_Carrier_rope4 = ropeCreate [_aircraft, [0,0,0], Kauk_Logistics_Carrier, [-1.5,-1.5,0]]; //sad

ropeUnwind [Kauk_Logistics_Carrier_rope1, 1, -1, true];
ropeUnwind [Kauk_Logistics_Carrier_rope2, 1, -1, true];

_cargo attachTo [Kauk_Logistics_Carrier, [0,0,0]];

_name_cargo  = getText (configFile >> "cfgVehicles" >> typeof _cargo >> "displayName");
_aircraft vehicleChat format ["%1 lifted", _name_cargo];
Kauk_Logistics_CargoLifted = _cargo;

if (Kauk_Logistics_isLifted_Modern == 1) then {
  deleteVehicle Kauk_Logistics_Carrier;
  _ropes = ropes (vehicle player);
  {ropeDestroy _x;} count _ropes;
} else {
  detach Kauk_Logistics_CargoLifted;
};

_name_cargo = getText (configFile >> "cfgVehicles" >> typeof Kauk_Logistics_CargoLifted >> "displayName");
vehicle player vehicleChat format ["%1 dropped", _name_cargo];

if (Kauk_Logistics_CargoLifted isKindOf "Static") then {_obj_fall = [Kauk_Logistics_CargoLifted] spawn Kauk_Logistics_obj_fall;} else {
  _vel = velocity (vehicle player);
  Kauk_Logistics_CargoLifted setVelocity _vel;
};

Kauk_Logistics_CargoLifted = ObjNull;
Kauk_Logistics_Carrier = ObjNull;
Kauk_Logistics_isLifted = 0;
Kauk_Logistics_isLifted_Modern = 0;
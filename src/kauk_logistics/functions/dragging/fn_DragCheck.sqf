if (!(vehicle player isKindOf "Man") || kauklog_Drag_isDragging) exitWith {false};
_checkStatus = false;
_blacklistallow = true;
_ins = lineIntersectsSurfaces [
  AGLToASL positionCameraToWorld [0,0,0],
  AGLToASL positionCameraToWorld [0,0,10],
  player,
  objNull,
  true,
  1,
  "GEOM",
  "NONE"
];
_object = (_ins select 0 select 2);
if (!(isNil {_object})) then {
  { //Blacklist Check (If triggered, Will not show even if object is also in whitelist)
    if (_object isKindOf _x) then { _blacklistallow = false;};
  } count Kauk_log_dragObjectsBlackList;

  if (_blacklistallow) then {
    { //Whitelist Check
       if (_object isKindOf _x) then { _checkStatus = true;};
    } count Kauk_log_dragObjects;
  };
};
_checkStatus
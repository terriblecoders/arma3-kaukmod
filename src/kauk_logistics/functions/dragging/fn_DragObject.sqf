_ins = lineIntersectsSurfaces [
  AGLToASL positionCameraToWorld [0,0,0],
  AGLToASL positionCameraToWorld [0,0,10],
  player,
  objNull,
  true,
  1,
  "GEOM",
  "NONE"
];
_obj = (_ins select 0 select 2);
if (!(isNil {_obj})) then {
  kauklog_Drag_Obj = _obj;
  _pos = player worldToModel (_obj modelToWorld [0,0,0]);
  _obj attachTo [player, _pos];

  kauklog_Drag_isDragging = true;

  [_obj, _pos] spawn {
    _obj = _this select 0;
    _pos = _this select 1;
    _elev_cam_initial = acos ((ATLtoASL positionCameraToWorld [0, 0, 1] select 2) - (ATLtoASL positionCameraToWorld [0, 0, 0] select 2));
    kauklog_Drag_Stop = 0;
    while {kauklog_Drag_Stop == 0} do {
      sleep 0.05;
      _elev_cam = acos ((ATLtoASL positionCameraToWorld [0, 0, 1] select 2) - (ATLtoASL positionCameraToWorld [0, 0, 0] select 2));
      _diff = (_elev_cam_initial - _elev_cam) / 15;
      _pos_diff = (_pos select 2) + _diff;
      _obj attachTo [player, [_pos select 0, _pos select 1, _pos_diff]];
    };
  };
};
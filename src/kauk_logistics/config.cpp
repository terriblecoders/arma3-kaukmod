class CfgPatches {
  class kauk_logistics {
    units[] = {};
    weapons[] = {};
    author[] = {"{{ general.author }}"};
    authorUrl = "{{ general.author_url }}";
    requiredVersion = {{ general.required_version }};
    requiredAddons[] = {};
    version = {{ general.version }};
  };
};

class CfgFunctions {
  class kauklog {
    class Logistics {
      file = "\kauk_logistics\functions";
      class InitLogistics { preInit = 1; };
    };
    class Dragging {
      file = "\kauk_logistics\functions\dragging";
      class DragObject;
      class DragRelease;
      class DragCheck;
      {% if logistics.enable_rotation_function %}class DragRotate;{% endif %}
    };
    class Lifting {
      file = "\kauk_logistics\functions\lifting";
      class SlingObject;
      class ReleaseObject;
      class SlingObjectLegacy;
      class SlingCheck;
    };
    class Loading {
      file = "\kauk_logistics\functions\loading";
      class SelectObject;
      class CheckContent;
      class LoadObject;
      class UnloadContent;
      class LoadCheck;
    };
    class Attaching {
      file = "\kauk_logistics\functions\attaching";
      class AttachObject;
      class StopAttachObject;
      class DetachObject;
      class AlignObject;
    };
  };
};
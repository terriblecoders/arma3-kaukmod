class CfgPatches {
  class kauk_weapon_patches {
    units[] = {};
    weapons[] = {};
    author[] = {"{{ general.author }}"};
    authorUrl = "{{ general.author_url }}";
    requiredVersion = {{ general.required_version }};
    requiredAddons[] = {
      {%- if weapon_patches.riflegrenades -%}
        "asdg_jointrails", 
        "r3f_armes_c",
      {%- endif -%}
      {%- if weapon_patches.huntir -%}
        "ace_huntir",
      {%- endif -%}
      {%- if weapon_patches.flashlights -%}
        "ace_flashlights",
      {%- endif -%}
      "A3_Weapons_F"
    };
    version = {{ general.version }};
  };
};

class Mode_SemiAuto;
class Mode_FullAuto;

class CfgWeapons {
  class UGL_F;
  class Rifle_Base_F;
  class ItemCore;

  {#/*
    If custom_sounds is enabled, this section changes the Sounds of the following guns to sounds from Project Reality:

    * MX Rifles
    * F2000
    * Tavor 21

    Project reality is a mod that focuses on providing a military simulation within the Battlefield 2 Engine, which is an IP owned by Electronic Arts.
    All Assets within the sound folder are property of the Project Reality modding team.

    The sounds of Project Reality are covered under the Creative Commons license and thus are free to use if you don't make any profits off it.
    More about the licensing for Project reality can be found here: http://www.realitymod.com/licensing

    The license for the Sounds can be found here: http://creativecommons.org/licenses/by-nc-nd/3.0/
  */#}
  {%- if weapon_patches.custom_sounds -%}
    class arifle_MX_Base_F : Rifle_Base_F {
      class Single : Mode_SemiAuto {
        class SilencedSound {
          begin1[] = {"kauk_weapon_patches\sounds\MX\m4sd_fire",0.794328,1,400};
          soundBegin[] = {"begin1",1};
        };
        class StandardSound {
          begin1[] = {"kauk_weapon_patches\sounds\MX\gpmg_fire_1p.wss",3.16228,1,1800};
          soundBegin[] = {"begin1",1};
        };
      };
      class FullAuto : Mode_FullAuto {
        class SilencedSound {
          begin1[] = {"kauk_weapon_patches\sounds\MX\m4sd_fire",0.794328,1,400};
          soundBegin[] = {"begin1",1};
        };
        class StandardSound {
          begin1[] = {"kauk_weapon_patches\sounds\MX\gpmg_fire_1p.wss",3.16228,1,1800};
          soundBegin[] = {"begin1",1};
        };
      };
    };
    class arifle_MX_SW_F : arifle_MX_Base_F {
      class manual : FullAuto {
        class SilencedSound {
          begin1[] = {"kauk_weapon_patches\sounds\MX\m4sd_fire",0.794328,1,400};
          soundBegin[] = {"begin1",1};
        };
        class StandardSound {
          begin1[] = {"kauk_weapon_patches\sounds\MX\gpmg_fire_1p.wss",3.16228,1,1800};
          soundBegin[] = {"begin1",1};
        };
      };
    };
    class mk20_base_F : Rifle_Base_F {
      class Single : Mode_SemiAuto {
        class StandardSound {
          begin1[] = {"kauk_weapon_patches\sounds\F2000\hk416_fire_1p.wss",2.51189,1,1400};
          soundBegin[] = {"begin1",1};
        };
      };
      class FullAuto : Mode_FullAuto {
        class StandardSound {
          begin1[] = {"kauk_weapon_patches\sounds\F2000\hk416_fire_1p.wss",2.51189,1,1400};
          soundBegin[] = {"begin1",1};
        };
      };
    };
    class Tavor_base_F : Rifle_Base_F {
      class Single : Mode_SemiAuto {
        class StandardSound {
          begin1[] = {"kauk_weapon_patches\sounds\Tar\tar21_fire_1p.wss",1.99526,1,1400};
          soundBegin[] = {"begin1",1};
        };
      };
      class FullAuto : Mode_FullAuto {
        class StandardSound {
          begin1[] = {"kauk_weapon_patches\sounds\Tar\tar21_fire_1p.wss",1.99526,1,1400};
          soundBegin[] = {"begin1",1};
        };
      };
    };
  {%- endif -%}

 class CannonCore;
  class Item_Base_F;
  class Item_Laserdesignator : Item_Base_F {
    visionMode[] = {"Normal","NVG", "Ti"};    //Add Ti Option to Laserdesignator Again. I like the option of having Ti on it.
  };

  {%- if weapon_patches.huntir -%}{% include "./includes/huntir.hpp" %}{%- endif -%}
  {%- if weapon_patches.flashlights -%}{% include "./includes/flashlights.hpp" %}{%- endif -%}
};
{%- if weapon_patches.riflegrenades -%}{% include "./includes/r3f_riflegrenades.hpp" %}{%- endif -%}
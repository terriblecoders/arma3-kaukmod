{#/*

This include contains the R3F Riflegrenade classnames which are added to all ASDG JR Rifles

*/#}
class compatibleItems {
  R3F_APAV40 = 1;
  R3F_AC58 = 1;
  R3F_FUM40 = 1;
  R3F_ECL40 = 1;
};
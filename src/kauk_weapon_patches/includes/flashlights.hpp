{#/*

This include makes various flashlights from other mods compatible with the ACE Flashlight feature.
This feature allows the flashlights to be used to light up the map in darkness when nothing else is available.

*/#}

class InventoryFlashlightItem_Base_F;

class RH_SFM952V : ItemCore { //Robert Hammer Surefire Flashlight
  class ItemInfo: InventoryFlashlightItem_Base_F {
    class Flashlight {
      ACE_Flashlight_Colour = "white";
      ACE_Flashlight_Size = 2.15;
    };
  };
};

class CUP_acc_Flashlight : ItemCore { //Regular CUP Flashlight
  class ItemInfo: InventoryFlashlightItem_Base_F {
    class Flashlight {
      ACE_Flashlight_Colour = "white";
      ACE_Flashlight_Size = 2.15;
    };
  };
};
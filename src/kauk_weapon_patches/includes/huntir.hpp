{#/*

This include makes various Rifles with Gl attachments compatible with the ACE HuntIR Grenade.

https://github.com/acemod/ACE3/tree/master/addons/huntir

The HuntIR is a GL-Launched Camera System which can be used to observe the battlefield from above without exposing the player to enemy fire.

*/#}

//Robert Hammers M16's and M4's
class RH_m4 : Rifle_Base_F {
  class  M203 : UGL_F {
    magazines[] += {"ACE_HuntIR_M203"};
  };
};

//CUP M16
class CUP_arifle_M16_Base : Rifle_Base_F {
  class M203 : UGL_F {
    magazines[] += {"ACE_HuntIR_M203"};
  };
};

//CUP SCAR
class CUP_arifle_SCAR_Base : Rifle_Base_F {
  class EGLMMuzzle : UGL_F {
    magazines[] += {"ACE_HuntIR_M203"};
  };
};

//CUP XM8
class CUP_arifle_XM8_Base : Rifle_Base_F {
  class XM320Muzzle : UGL_F {
    magazines[] += {"ACE_HuntIR_M203"};
  };
};

//CUP L85
class CUP_arifle_L85A2_Base : Rifle_Base_F {
  class BAF_L17_40mm : UGL_F {
    magazines[] += {"ACE_HuntIR_M203"};
  }
};

//RHS AK's
class rhs_weap_ak74m_Base_F : Rifle_Base_F {
  class GP25Muzzle : UGL_F {
    magazines[] += {"ACE_HuntIR_M203"};
  };
};

//R3F Famas
class R3F_Famas_F1 : Rifle_Base_F {
  class Lance_Grenades : UGL_F {
    magazines[] += {"ACE_HuntIR_M203"};
  };
};

//CUP Standalone Launchers
class CUP_glaunch_Base : Rifle_Base_F {
  magazines[] += {"ACE_HuntIR_M203"};
};

//RHS M320
class rhs_weap_M197 : CannonCore {};
class rhs_weap_M230 : rhs_weap_M197 {
  magazines[] += {"ACE_HuntIR_M203"};
}
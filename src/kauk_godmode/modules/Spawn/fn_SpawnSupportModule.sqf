/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_SpawnSUpportModule
|----------------------------------------------------------------------
|Description:
|Spawns all Support modules
|
|Usage:
|Select module then click anywhere on ground. A Menu should open
|in which you can select the desired module to spawn.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/
#include "\kauk_godmode\module_header.hpp"

_pos = getPos _logic;

_center = createCenter sideLogic;
_logicGroup = createGroup _center;
_requester = _logicGroup createUnit ["SupportRequester",getpos player, [], 0, "FORM"];

_provider1 = _logicGroup createUnit ["SupportProvider_Virtual_Drop", _pos, [], 0, "FORM"];
_provider2 = _logicGroup createUnit ["SupportProvider_Virtual_Artillery", _pos, [], 0, "FORM"];
_provider3 = _logicGroup createUnit ["SupportProvider_Virtual_CAS_Bombing", _pos, [], 0, "FORM"];
_provider4 = _logicGroup createUnit ["SupportProvider_Virtual_CAS_Heli", _pos, [], 0, "FORM"];
_provider5 = _logicGroup createUnit ["SupportProvider_Virtual_Transport", _pos, [], 0, "FORM"];

{[_requester, _x, -1] call BIS_fnc_limitSupport;} count ["Artillery","CAS_Heli","CAS_Bombing","UAV","Drop","Transport"];
{[player, _requester, _x] call BIS_fnc_addSupportLink;} count [_provider1, _provider2, _provider3, _provider4, _provider5];

[objNull, "Spawned Support Modules"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
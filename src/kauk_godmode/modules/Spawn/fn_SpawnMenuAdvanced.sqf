#include "\kauk_godmode\module_header.hpp"

_pos = getPos _logic;

_cfgVehicles_categories = [];
_cfgVehicles_categories_toLower = [];
_nb_config = count (configFile >> "CfgVehicleClasses");
for [{_idx_config = 0}, {_idx_config < _nb_config}, {_idx_config = _idx_config+1}] do
{
  _config = (configFile >> "CfgVehicleClasses") select _idx_config;
  if (isClass _config) then
  {
      _cfgVehicles_categories pushBack (configName _config);
      _cfgVehicles_categories_toLower pushBack (toLower configName _config);
  };
};

//Get the Object Category Names
_object_category_names = [];
{
  _null = _object_category_names pushBack (getText (configfile >> "CfgVehicleClasses" >> _x >> "displayName"));
} count _cfgVehicles_categories;

_dialogResult =
  [
    "Spawn Menu",
    [
      ["Select Category", _object_category_names]
    ]
  ] call Kauk_fnc_ShowChooseDialog;

_object_category_selected = _cfgVehicles_categories select (_dialogResult select 0);

//Get Array of Objects inside Category
_cfgVehicles_par_categories = [];
_nb_config = count (configFile >> "CfgVehicles");
for [{_idx_config = 0}, {_idx_config < _nb_config}, {_idx_config = _idx_config+1}] do
{
  _config = (configFile >> "CfgVehicles") select _idx_config;
  if (isClass _config) then
  {
    if (getNumber (_config >> "scope") == 2 && !(configName _config isKindOf "Man")) then
    {
      _idx_categorie = (toLower _object_category_selected) find (toLower getText (_config >> "vehicleClass"));
      if (_idx_categorie != -1) then
      {
        _cfgVehicles_par_categories pushBack (configName _config);
      };
    };
  };
};

//Get the Object Selection Names
_object_selection_names = [];
{
  _null = _object_selection_names pushBack (getText (configfile >> "CfgVehicles" >> _x >> "displayName"));
} count _cfgVehicles_par_categories;

_dialogResult =
  [
    "Spawn Menu",
    [
      ["Select Object", _object_selection_names]
    ]
  ] call Kauk_fnc_ShowChooseDialog;

_object_selected = _cfgVehicles_par_categories select (_dialogResult select 0);
_obj = _object_selected createVehicle _pos;
[[_obj]] call Kauk_fnc_AddUnitsToCurator;

[objNull, "Spawned Object"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
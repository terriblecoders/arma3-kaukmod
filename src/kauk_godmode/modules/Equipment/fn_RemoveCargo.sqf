/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_RemoveCargo
|----------------------------------------------------------------------
|Description:
|removes all items in vehicle cargo
|
|Usage:
|Select module, then click on Vehicle.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
clearMagazineCargoGlobal _object;
clearBackpackCargoGlobal _object;
clearItemCargoGlobal _object;
clearWeaponCargoGlobal _object;

[objNull, "Removed Cargo from Object"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_RemoveInventory
|----------------------------------------------------------------------
|Description:
|removes all items in unit inventory.
|
|Usage:
|Select module, then click on unit.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
removeAllWeapons _object;
removeAllItems _object;
removeAllAssignedItems _object;
removeUniform _object;
removeVest _object;
removeBackpack _object;
removeHeadgear _object;
removeGoggles _object;

[objNull, "Removed All Items from Unit"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
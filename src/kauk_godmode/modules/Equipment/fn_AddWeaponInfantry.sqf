/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_AddWeapon
|----------------------------------------------------------------------
|Description:
|Adds a Weapon and it's assigned magazines to a vehicle.
|
|Usage:
|In game, select module and click on vehicle. Then select weapon and
|amount of ammo. Does not work on units.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

if (STATE_AddWeaponInfantryCache) then {
  _cfgWeapons_categories = [];
  _cfgWeapons_categories_names = [];
  for [{_config_index = 0}, {_config_index < (count (configFile >> "CfgWeapons"))}, {_config_index = _config_index+1}] do {
    _config = (configFile >> "CfgWeapons") select _config_index;
    if (isClass _config) then {
      _name = configName _config;
      _displayname = (getText (configfile >> "CfgWeapons" >> _name >> "displayName"));
      if (_name isKindOf ["RifleCore", configFile >> "CfgWeapons"]) then {
        if (!(_displayname == "")) then {
          _cfgWeapons_categories pushBack _name;
          _cfgWeapons_categories_names pushBack format["%1    (%2)", _name, _displayname];
        };
      };
    };
  };
  STATE_AddWeaponInfantryCache = false;
  AddWeaponInfantry = _cfgWeapons_categories call BIS_fnc_sortAlphabetically;
  AddWeaponInfantry_names = _cfgWeapons_categories_names call BIS_fnc_sortAlphabetically;
};

_dialogResult = [
  "Add Weapon to vehicle:",
  [
    ["Weapon",AddWeaponInfantry_names],
    ["Ammo Multiplier", ["1", "2", "5", "10", "15"]]
  ]
] call Kauk_fnc_ShowChooseDialog;

_mag_multiplier = 1;
switch (_dialogResult select 1) do {
  case 0: { _mag_multiplier = 1; };
  case 1: { _mag_multiplier = 2; };
  case 2: { _mag_multiplier = 5; };
  case 3: { _mag_multiplier = 10; };
  case 4: { _mag_multiplier = 15; };
  default { _mag_multiplier = 1; };
};

_weapon = AddWeaponInfantry select (_dialogResult select 0);

_mag_array_DE = getArray (configfile >> "CfgWeapons" >> _weapon >> "magazines");          //Default Magazine config
_mag_array_AP = getArray (configfile >> "CfgWeapons" >> _weapon >> "AP" >> "magazines");  //AP Magazines
_mag_array_HE = getArray (configfile >> "CfgWeapons" >> _weapon >> "HE" >> "magazines");  //HE Magazines
_mag_array_AT = getArray (configfile >> "CfgWeapons" >> _weapon >> "AT" >> "magazines");  //AT Magazines
_mag_array = _mag_array_DE + _mag_array_AP + _mag_array_HE + _mag_array_AT;

for [{_i=0}, {_i<_mag_multiplier}, {_i=_i+1}] do {
  {
    if (_x != "") then {
      _object addMagazine _x;
    };
  } count _mag_array;
};

_object addWeapon _weapon;

[objNull, "Gun Added"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
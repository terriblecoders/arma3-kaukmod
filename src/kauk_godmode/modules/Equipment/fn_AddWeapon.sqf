/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_AddWeapon
|----------------------------------------------------------------------
|Description:
|Adds a Weapon and it's assigned magazines to a vehicle.
|
|Usage:
|In game, select module and click on vehicle. Then select weapon and
|amount of ammo. Does not work on units.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_options = [
  ["Custom CfgWeapon", option_CfgWeapon],
  ["---- Big Cannons ----",""],
  ["30mm Gatling CAS","Gatling_30mm_Plane_CAS_01_F"],
  ["30mm Gatling","gatling_30mm"],
  ["20mm Gatling","gatling_20mm"],
  ["120mm Cannon","cannon_120mm"],
  ["40mm Grenade Launcher","GMG_40mm"],
  ["155mm Mortar","mortar_155mm_AMOS"],
  ["35mm Autocannon","autocannon_35mm"],
  ["30mm Autocannon","autocannon_30mm"],
  ["40mm Autocannon","autocannon_40mm_CTWS"],
  ["82mm Mortar", "mortar_82mm"],
  ["CTWS Cannon 30 mm", "autocannon_30mm_CTWS"],
  ["20mm Twin Cannon", "Twin_Cannon_20mm"],
  ["Gau-8/A Avenger (RHS)","RHS_weap_gau8"],
  ["2A42 (RHS)","rhs_weap_2a42"],
  ["M-230 chain gun (RHS)","rhs_weap_m230"],
  ["M242 BC (RHS)","RHS_weap_M242BC"],
  ["2A46 T80 Cannon (RHS)","rhs_weap_2a46_2"],
  ["L/44 M256A1 120mm (RHS)", "rhs_weap_m256"],
  ["155mm cannon (RHS)", "mortar_155mm_amos"],
  ["GSh-30 (RHS)", "rhs_weap_gsh30"],
  ["M61 (F/A-18)", "js_w_fa18_m61"],
  ["GSH-30 (SU-35)", "js_w_su35_gsh30"],

  ["---- Small Cannons----",""],
  ["6.5mm LMG","LMG_M200"],
  ["6.5mm Minigun","LMG_Minigun"],
  ["6.5mm Minigun Heli","LMG_Minigun_heli"],
  ["12.7mm HMG","HMG_127_APC"],
  [".50 HMG","HMG_01"],
  ["NSVT HMG","HMG_NSVT"],
  ["ZSU Shilka (RHS)","RHS_weap_AZP23"],
  ["KVPT MG (RHS)","rhs_weap_kpvt"],

  ["---- Rockets ----",""],
  ["DAR Missiles","missiles_DAR"],
  ["Skyfire Rockets","rockets_Skyfire"],
  ["230mm Rockets","rockets_230mm_GAT"],
  ["Hydra 70mm Rockets","Rocket_04_HE_Plane_CAS_01_F"],
  ["S8 Launcher (RHS)","rhs_weap_s8"],
  ["LAU-61 FFAR (RHS)","rhs_weap_FFARLauncher"],
  ["122mm Grad (RHS)","rhs_weap_grad"],
  ["S8 (SU-35)", "js_w_su35_s8Laucher"],

  ["---- Missiles ----",""],
  ["Titan Launcher","missiles_titan"],
  ["DAGR Missiles","missiles_DAGR"],
  ["ASRAAM Missiles","missiles_ASRAAM"],
  ["SCALPEL Missiles","missiles_SCALPEL"],
  ["AGM-65 Mavericks Missiles","Missile_AGM_02_Plane_CAS_01_F"],
  ["TOW Wired (RHS)","Rhs_weap_TOW_Launcher"],
  ["AGM-65 Maverick (RHS)","rhs_weap_agm65"],
  ["AIM-9 Sidewinder (RHS)","rhs_weap_sidewinderlauncher"],
  ["R-73 Wired Launcher (RHS)","rhs_weap_r73_Launcher"],
  ["FIM 92 Stinger (RHS)","rhs_weap_fim92"],
  ["9K114 Kokon / 9M120 Ataka Wired (RHS)", "rhs_weap_9K114_launcher"],
  ["Vikhr (RHS)", "rhs_weap_Vikhr_Launcher"],
  ["AIM120 (F/A-18)", "js_w_fa18_aim120cLaucher"],
  ["AIM 9x (F/A-18)", "js_w_fa18_aim9xLaucher"],
  ["Harpoon (F/A-18)", "js_w_fa18_HarpoonLauncher"],
  ["AGM-65 Maverick (F/A-18)", "js_w_fa18_MaverickLauncher"],
  ["KH-29 (SU-35)", "js_w_su35_kh29Launcher"],
  ["R73 Vympel (SU-35)", "js_w_su35_r73Laucher"],
  ["R77 Vympel (SU-35)", "js_w_su35_r77Laucher"],

  ["---- Bombs ----",""],
  ["GBU12 LGB", "GBU12BombLauncher"],
  ["LOM-250G", "Bomb_03_Plane_CAS_02_F"],
  ["LOM-500G", "Bomb_04_Plane_CAS_01_F"],
  ["FAB250 (RHS)", "rhs_weap_fab250"],
  ["GBU12 LGB (F/A-18)", "js_w_fa18_GBU12LGBLaucher"],
  ["GBU31 (F/A-18)", "js_w_fa18_GBU31BombLauncher"],
  ["GBU32 (F/A-18)", "js_w_fa18_GBU32BombLauncher"],
  ["GBU38 (F/A-18)", "js_w_fa18_GBU38BombLauncher"],
  ["MK82 (F/A-18)", "js_w_fa18_Mk82BombLauncher"],
  ["KAB500l (SU-35)", "js_w_su35_kab500lLaucher"],

  ["---- Other ----",""],
  ["Laser designator","Laserdesignator_mounted"],
  ["Smoke Launcher", "SmokeLauncher"],
  ["Flare Countermeasures","CMFlareLauncher"],
  ["Searchlight", "SEARCHLIGHT"],

  ["", ""]
];

_optionNames = [];
{
  _null = _optionNames pushBack (_x select 0);
} count _options;

_dialogResult = [
  "Add Weapon to vehicle:",
  [
    ["Weapon",_optionNames],
    ["Ammo Multiplier", ["1", "2", "5", "10", "15"]]
  ]
] call Kauk_fnc_ShowChooseDialog;

_mag_multiplier = 1;
switch (_dialogResult select 1) do {
  case 0: { _mag_multiplier = 1; };
  case 1: { _mag_multiplier = 2; };
  case 2: { _mag_multiplier = 5; };
  case 3: { _mag_multiplier = 10; };
  case 4: { _mag_multiplier = 15; };
  default { _mag_multiplier = 1; };
};

_weapon = (_options select (_dialogResult select 0)) select 1;

_mag_array_DE = getArray (configfile >> "CfgWeapons" >> _weapon >> "magazines");          //Default Magazine config
_mag_array_AP = getArray (configfile >> "CfgWeapons" >> _weapon >> "AP" >> "magazines");  //AP Magazines
_mag_array_HE = getArray (configfile >> "CfgWeapons" >> _weapon >> "HE" >> "magazines");  //HE Magazines
_mag_array_AT = getArray (configfile >> "CfgWeapons" >> _weapon >> "AT" >> "magazines");  //AT Magazines
_mag_array = _mag_array_DE + _mag_array_AP + _mag_array_HE + _mag_array_AT;

for [{_i=0}, {_i<_mag_multiplier}, {_i=_i+1}] do {
  {
    if (_x != "") then {
      _object addMagazine _x;
    };
  } count _mag_array;
};

_object addWeapon _weapon;

[objNull, "Gun Added"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
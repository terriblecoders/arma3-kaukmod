/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_CopyLoadout
|----------------------------------------------------------------------
|Description:
|removes all items in unit inventory.
|
|Usage:
|Select module, then click on unit.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_object call Kauk_fnc_loadoutCopy;
[objNull, "Copied Loadout to other units"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
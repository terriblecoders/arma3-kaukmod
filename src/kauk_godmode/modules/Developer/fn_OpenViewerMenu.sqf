/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_OpenViewerMenu
|----------------------------------------------------------------------
|Description:
|Opens up the viewer menu from which you can select different dev
|menus.
|
|Usage:
|Select module, then click on anything to open menu.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/


#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_dialogResult = [
  "Developer Views",
  [
    "Open View",
    [
      "Open Config Viewer",
      "Open Function Viewer",
      "Open GUI Editor",
      "Open BI Debug Menu",
      "Open Splendid Camera"
    ]
  ]] call Kauk_fnc_ShowChooseDialog;

switch (_dialogResult select 0) do {
  case 0: { [] call BIS_fnc_configviewer };
  case 1: { [] call BIS_fnc_help };
  case 2: { [] call BIS_fnc_GUIeditor };
  case 3: { (finddisplay 46) createdisplay 'RscDisplayDebugPublic'; };
  case 4: { [] call bis_fnc_camera; };
  default { hint "Error"; };
};

#include "\kauk_godmode\module_footer.hpp"
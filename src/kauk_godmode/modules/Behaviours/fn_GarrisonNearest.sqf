#include "\kauk_godmode\module_header.hpp"

_groupUnderCursor = [_logic] call Kauk_fnc_GetGroupUnderCursor;

_doesGroupContainAnyPlayer = false;
{
  if (isPlayer _x) exitWith { _doesGroupContainAnyPlayer = true; };
} count (units _groupUnderCursor);

if (_doesGroupContainAnyPlayer) then
{
  [objnull, "Cannot garrison groups containing playable units."] call bis_fnc_showCuratorFeedbackMessage;
}
else
{
  [(getPos _logic), (units _groupUnderCursor), 150, true, false] call Kauk_fnc_ZenOccupyHouse;
  [objnull, "Garrisoned nearest building."] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
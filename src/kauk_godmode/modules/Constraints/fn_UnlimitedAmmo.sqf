/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_UnlimitedAmmo
|----------------------------------------------------------------------
|Description:
|Unit or vehicle now has unlimited ammo
|
|Usage:
|Select module, then click on object.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_object addeventhandler ["fired", {(_this select 0) setvehicleammo 1}];

[objNull, "Added unlimited Ammo"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
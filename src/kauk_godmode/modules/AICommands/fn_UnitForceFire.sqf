/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_UnitForceFire
|----------------------------------------------------------------------
|Description:
|Forces an AI unit to fire it's current weapon
|
|Usage:
|Select module and click on unit which you want to fire his weapon
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;

_weapon = currentWeapon _object;
_firemodes = (getArray (configFile >> "CfgWeapons" >> _weapon >> "modes"));
_firemode = _firemodes select 0;
if (_firemode == "this") then {
  //This is for ordnances like Rocket launchers who have "This" as firemode
  _object forceWeaponFire [_weapon, _weapon];
} else {
  //For things with regular firemodes lke "Single, Burst, etc..."
  _object forceWeaponFire [_weapon, _firemodes select 0];
};

[objNull, "AI forced to use Current Gun."] call bis_fnc_showCuratorFeedbackMessage;


#include "\kauk_godmode\module_footer.hpp"
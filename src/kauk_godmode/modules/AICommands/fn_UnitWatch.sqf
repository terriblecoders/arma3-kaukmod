/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_UnitWatch
|----------------------------------------------------------------------
|Description:
|Make the unit watch a certain position
|
|
|Usage:
|Select module and click on the unit and then on the position to watch
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_state = initialPlayer getVariable "Kauk_ModuleState";

if (_state == 0) then {
  //New State
  _object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
  lookAtSource = _object;
  initialPlayer setVariable ["Kauk_ModuleState", 1]; //Set state to 1, AKA Waiting for second option
  [objNull, format["Set Object"]] call bis_fnc_showCuratorFeedbackMessage;
} else {
  //Second State
  _pos = getPos _logic;
  lookAtSource doWatch _pos;
  initialPlayer setVariable ["Kauk_ModuleState", 0]; //Set state to 0, AKA new state
  [objNull, "Object watching position"] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
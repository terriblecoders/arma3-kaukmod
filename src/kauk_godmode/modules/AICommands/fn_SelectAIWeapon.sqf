/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_SelectAIWeapon
|----------------------------------------------------------------------
|Description:
|Forces an AI to select a certain weapon.
|
|Usage:
|Select module and click on AI and then choose which weapon to use
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_vehicle = vehicle _object;
_weapons = (weapons _vehicle);

_weapons_names = [];
{
  _weapons_names pushBack (getText (configfile >> "CfgWeapons" >> _x >> "displayName"));
} count _weapons;

_dialogResult =
  [
    "AI Weapon Select",
    [
      ["Select Weapon...", _weapons_names]
    ]
  ] call Kauk_fnc_ShowChooseDialog;

if ((count _dialogResult) > 0) then
{
  _wep = _weapons select (_dialogResult select 0);
  _vehicle selectWeapon _wep;
  [objNull, format["Weapon: %1 Selected.", _wep]] call bis_fnc_showCuratorFeedbackMessage;
} else {
  [objNull, "No Weapon Found"] call bis_fnc_showCuratorFeedbackMessage;
};

#include "\kauk_godmode\module_footer.hpp"
#include "\kauk_godmode\module_header.hpp"

// Generate list of the players to teleport.
_playersToTeleport = [];
{
  _playersToTeleport set [count _playersToTeleport, _x];
} count allPlayers;

// Get the location to teleport them to.
_location = getPos _logic;

// Call the teleport function.
[_playersToTeleport, _location] call Kauk_fnc_TeleportPlayers;

[objNull, format["Teleported %1 players to %2", (count _playersToTeleport), _location]] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
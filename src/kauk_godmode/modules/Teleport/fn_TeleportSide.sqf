/*
|----------------------------------------------------------------------
|Name: Kauk_fnc_TeleportSide
|----------------------------------------------------------------------
|Description:
|Teleports everyone from a certain side
|
|Usage:
|Select module, then click on unit. A Dialogue opens up in which you
|can select the side for unit teleport.
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

#include "\kauk_godmode\module_header.hpp"

_dialogResult = [
  "Teleport Side",
  [
    "Select Side",
    [
      "West",
      "East",
      "Indep",
      "Civ"
    ]
  ]] call Kauk_fnc_ShowChooseDialog;

_side = civilian;
switch (_dialogResult select 0) do {
  case 0: {_side = west;};
  case 1: {_side = east;};
  case 2: {_side = resistance;};
  case 3: {_side = civilian;};
  default { hint "Error"; };
};

// Generate list of the players to teleport.
_playersToTeleport = [];
{
  if (isPlayer _x && (side _x) == _side) then
  {
    _playersToTeleport set [count _playersToTeleport, _x];
  };
} count allUnits;

// Get the location to teleport them to.
_location = getPos _logic;

// Call the teleport function.
[_playersToTeleport, _location] call Kauk_fnc_TeleportPlayers;

[objNull, format["Teleported %1 players to %2", (count _playersToTeleport), _location]] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"
#include "\kauk_godmode\module_header.hpp"

_dialogResult = [
  "Spawn LHD",
  [
    "Spawn...",
    [
      "AllInArma LHD",
      "Atlas LHD"
    ],1
  ]
] call Kauk_fnc_ShowChooseDialog;

if (count _dialogResult > 0) then
{
  _ship = 0;
  switch (_dialogResult select 0) do
  {
    case 0: { _ship = 0; };
    case 1: { _ship = 1; };
    default { _ship = 0; };
  };

  //Get the Position
  _pos_lhd = getPos _logic;

  if (_ship == 0) then { //Spawn the AllInArma Variant of the LHD
    LHDComponent1 = "Land_LHD_1" createVehicle _pos_lhd;
    LHDComponent2 = "Land_LHD_2" createVehicle _pos_lhd;
    LHDComponent3 = "Land_LHD_3" createVehicle _pos_lhd;
    LHDComponent4 = "Land_LHD_4" createVehicle _pos_lhd;
    LHDComponent5 = "Land_LHD_5" createVehicle _pos_lhd;
    LHDComponent6 = "Land_LHD_6" createVehicle _pos_lhd;
    LHDComponentElevator = "Land_LHD_elev_r" createVehicle _pos_lhd;
    LHDComponentIsland1 = "Land_LHD_house_1" createVehicle _pos_lhd;
    LHDComponentIsland2 = "Land_LHD_house_2" createVehicle _pos_lhd;
    {
      _x setPos _pos_lhd
    } count [LHDComponent1, LHDComponent2, LHDComponent3, LHDComponent4, LHDComponent5, LHDComponent6, LHDComponentElevator, LHDComponentIsland1, LHDComponentIsland2];
  } else { //Spawn the Atlas Variant of the LHD
    [_pos_lhd, 0] call Kauk_fnc_SetupLHD;
  };
};

[objNull, "Created LHD"] call bis_fnc_showCuratorFeedbackMessage;

#include "\kauk_godmode\module_footer.hpp"

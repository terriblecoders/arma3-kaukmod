#include "\kauk_godmode\module_header.hpp"

/*

POS_DEBUG = KAUKMOD_LHD worldToModel (getPosASL player); hint str POS_DEBUG;
copyToClipboard str POS_DEBUG;

Get more Positions with the following code

//Helpers
16.71 = Height for Top Floor of LHD
7.9 = Height of middle cargo plattform (Lower Part)
2.2 Height of LHD Dock (Upper Part)

*/

_object = [_logic] call Kauk_fnc_GetUnitUnderCursor;
_dialogResult = [
  "Spawn LHD",
  [
    "Spawn...",
    [
      "Player Spot",
      "Runway Back",
      "Lift Back (Top)",
      "Runway Middle",
      "Top Cargo 1",
      "Top Cargo 2",
      "Bottom Cargo 1",
      "Bottom Cargo 2",
      "Dock Top",
      "Dock Water"
    ],0
  ]
] call Kauk_fnc_ShowChooseDialog;

if (count _dialogResult > 0) then {

  _pos = [0,0,0];
  _dir = 0;
  switch (_dialogResult select 0) do
  {
    case 0: { _pos = [0, 0, 16.71]; _dir = 90; };   //Player Spawn
    case 1: { _pos = [9, 100, 16.71]; _dir = 180; }; //Runway Back
    case 2: { _pos = [-22,91,16.71]; _dir = 90; };   //Back Lift (Top)
    case 3: { _pos = [9, 0, 16.71]; _dir = 0; };    //Runway Middle
    case 4: { _pos = [-7, 52, 16.71]; _dir = 90; }; // Top Cargo 1
    case 5: { _pos = [-7, 60, 16.71]; _dir = 90; }; // Top Cargo 2
    case 6: { _pos = [1, 80, 7.9]; _dir = 180; }; //Bottom Cargo 1
    case 7: { _pos = [1, 40, 7.9]; _dir = 180; }; //Bottom Cargo 2
    case 8: { _pos = [1, 30, 2.2]; _dir = 0; }; //Dock Top
    case 9: { _pos = [1, 64, 0]; _dir = 0; }; //Dock Water
    default { _pos = [0, 0, 16.71]; _dir = 180; };
  };

  if (!(_object isKindOf "Man")) then {
    if (_object isKindOf "AllVehicles") then {
      _object enableSimulation false;
      _object enableSimulationGlobal false;
      _object allowDamage false;
      _action = [
        "lhd_unfreeze",
        "Unfreeze Object",
        "",
        {
          _obj = (_this select 0);
          _obj enableSimulation true;
          _obj enableSimulationGlobal true;
          _obj allowDamage true;
          [_obj,0,["ACE_MainActions","lhd_unfreeze"]] call ace_interact_menu_fnc_removeActionFromObject;
        },
        {true}
      ] call ace_interact_menu_fnc_createAction;
      [_object, 0, ["ACE_MainActions"], _action] call ace_interact_menu_fnc_addActionToObject;
    };

    if (_object isKindOf "Plane") then {
      _object enableSimulation false;
      _object enableSimulationGlobal false;
      _object allowDamage false;
      _action = [
        "lhd_startPlane",
        "Launch Plane",
        "",
        {
          _speed = 80;
          _object = (_this select 0);

          _object enableSimulation true;
          _object enableSimulationGlobal true;
          _object allowDamage true;

          _vel = velocity _object;
          _dirv = direction _object;
          _object setVelocity [
            (_vel select 0) + (sin _dirv * _speed),
            (_vel select 1) + (cos _dirv * _speed),
            (_vel select 2) + (_speed / 2)
          ];
          [_obj,0,["ACE_SelfActions","lhd_startPlane"]] call ace_interact_menu_fnc_removeActionFromObject;
        },
        {true}
      ] call ace_interact_menu_fnc_createAction;
      [_object, 1, ["ACE_SelfActions"], _action] call ace_interact_menu_fnc_addActionToObject;
    };
  };

  [_object, _pos, _dir] call Kauk_fnc_SetLHDRelObj;
};

#include "\kauk_godmode\module_footer.hpp"

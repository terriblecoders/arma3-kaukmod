class Kauk_Module_Spawn_LHD : Kauk_LHD_Module_Base {
  scopeCurator = 2;
  displayName = "Spawn LHD";
  function = "Kauk_fnc_SpawnLHD";
};

class Kauk_Module_Place_Object : Kauk_LHD_Module_Base {
  scopeCurator = 2;
  displayName = "Place Object on LHD";
  function = "Kauk_fnc_PlaceObjectOnLHD";
};


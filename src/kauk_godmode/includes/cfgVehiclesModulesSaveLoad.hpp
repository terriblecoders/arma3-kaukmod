class Kauk_Module_Save_Objects_For_Paste : Kauk_SaveLoad_Module_Base {
  scopeCurator = 2;
  displayName = "Copy To Clipboard";
  function = "Kauk_fnc_SaveObjectsForPaste";
};

class Kauk_Module_Paste_Objects_Relative : Kauk_SaveLoad_Module_Base {
  scopeCurator = 2;
  displayName = "Paste At New Position";
  function = "Kauk_fnc_PasteObjectsRelative";
};

class Kauk_Module_Paste_Objects_Absolute: Kauk_SaveLoad_Module_Base {
  scopeCurator = 2;
  displayName = "Paste At Original Position";
  function = "Kauk_fnc_PasteObjectsAbsolute";
};
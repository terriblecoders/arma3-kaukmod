class Kauk_Module_Teleport_All : Kauk_Teleport_Module_Base {
  scopeCurator = 2;
  displayName = "Teleport All";
  function = "Kauk_fnc_TeleportAllPlayers";
};

class Kauk_Module_Teleport_Single : Kauk_Teleport_Module_Base {
  scopeCurator = 2;
  displayName = "Teleport Single Unit";
  function = "Kauk_fnc_TeleportSinglePlayer";
};

class Kauk_Module_Teleport_Group : Kauk_Teleport_Module_Base {
  scopeCurator = 2;
  displayName = "Teleport Group";
  function = "Kauk_fnc_TeleportGroup";
};

class Kauk_Module_Teleport_Zeus : Kauk_Teleport_Module_Base {
  scopeCurator = 2;
  displayName = "Teleport Zeus";
  function = "Kauk_fnc_TeleportZeus";
};

class Kauk_Module_Teleport_Side : Kauk_Teleport_Module_Base {
  scopeCurator = 2;
  displayName = "Teleport Side";
  function = "Kauk_fnc_TeleportSide";
};

class Kauk_Module_Teleport_Zeus_Vehicle : Kauk_Teleport_Module_Base {
  scopeCurator = 2;
  displayName = "Teleport Zeus in Vehicle";
  function = "Kauk_fnc_TeleportZeusVehicle";
};
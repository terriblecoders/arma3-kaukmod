class Kauk_Module_Equipment_Nvgs : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Add/Remove NVG's";
  function = "Kauk_fnc_EquipmentNvgs";
};

class Kauk_Module_Equipment_Thermals : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Add/Remove Thermals";
  function = "Kauk_fnc_EquipmentThermals";
};

class Kauk_Module_Equipment_Lights : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Lights On/Off";
  function = "Kauk_fnc_EquipmentLights";
};

class kauk_Module_Equipment_Ordnance : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Remove Ordnance from Faction";
  function = "Kauk_fnc_RemoveOrdnance";
}

class Kauk_Module_Equipment_AddWeapon : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Add Vehicle Weapon";
  function = "Kauk_fnc_AddWeapon";
};

class Kauk_Module_Equipment_AddWeaponVehicles : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Add Weapon (Vehicle Weapons)";
  function = "Kauk_fnc_AddWeaponVehicles";
};
class Kauk_Module_Equipment_AddWeaponInfantry : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Add Weapon (Infantry Weapons)";
  function = "Kauk_fnc_AddWeaponInfantry";
};

class Kauk_Module_Equipment_RemoveWeapon : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Remove Vehicle / Unit Weapon";
  function = "Kauk_fnc_RemoveWeapon";
};

class Kauk_Module_Equipment_Remove_Inventory : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Remove Items from Unit";
  function = "Kauk_fnc_RemoveInventory";
};

class Kauk_Module_Equipment_RemoveCargo : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Remove Cargo";
  function = "Kauk_fnc_RemoveCargo";
};

class Kauk_Module_Equipment_CopyLoadout : Kauk_Equipment_Module_Base {
  scopeCurator = 2;
  displayName = "Copy Loadout to Group";
  function = "Kauk_fnc_CopyLoadout";
};
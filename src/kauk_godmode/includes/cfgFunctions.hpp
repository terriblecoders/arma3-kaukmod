class Kauk {

  /*

    General Functions

  */

  class init {
    file = "\kauk_godmode\functions\init";
    class RespawnEvent;
    class InitGodmode { preInit = 1; };
  };

  class events {
    file = "\kauk_godmode\functions\events";
    class OnModuleTreeLoad;
    class HandleCuratorObjectPlaced;
  };

  class ui {
    file = "\kauk_godmode\functions\UI";
    class CuratorPropertyActions;
    class ControlPanelActions;
    class AlignAttachment;
    class ShowChooseDialog;
    class DebugPos;
  };

  // Helper functions
  class util {
    file = "\kauk_godmode\functions";
    class AddUnitsToCurator;
    class BroadcastCode;
    class GetArrayDataFromUser;
    class GetGroupUnderCursor;
    class GetSafePos;
    class GetUnitUnderCursor;
    class GlobalExecute;
    class IsZeus;
    class MakePlayerInvisible;
    class MonitorCuratorDisplay;
    class ObjectsGrabber;
    class ObjectsMapper;
    class SearchBuilding;
    class TeleportPlayers;
    class ZenOccupyHouse;
    class ZeusRefresh;
    class ToggleReviveSystem;
    class SetAngle;
    class CreateLogic;
    class RemoteControl;
    class SwitchPlayer;
    class RefreshCuratorHandlers;
    class loadoutCopy;
  };

  class lhd {
    file = "\kauk_godmode\functions\LHD";
    class SetupLHD;
    class SetLHDRelObj;
  };

  /*

    Module Functions

  */


  class modules {
    file = "\kauk_godmode\modules";
    class Empty {};
  };

  class AICommandsModules {
    file = "\kauk_godmode\modules\AICommands";
    class ToggleAIAnim;
    class UnitForceFire;
    class UnitLook;
    class UnitLookObject;
    class SelectAIWeapon;
    class VehicleForceFire;
    class UnitWatch;
    class UnitWatchStop;
    class ArtilleryFire;
  };

  class ArsenalModules {
    file = "\kauk_godmode\modules\Arsenal";
    class ArsenalAddFull;
    class ArsenalAddEmpty;
  };

  class BehaviourModules {
    file = "\kauk_godmode\modules\Behaviours";
    class BehaviourPatrol;
    class BehaviourSearchNearbyAndGarrison;
    class BehaviourSearchNearbyBuilding;
    class GarrisonNearest;
    class UnGarrison;
  };

  class ConstraintsModules {
    file = "\kauk_godmode\modules\Constraints";
    class SetTexture;
    class AttachToObject;
    class DetachObject;
    class AttachRope;
    class DetachRopes;
    class ToggleDamage;
    class PushObject;
    class UnlimitedAmmo;
    class ToggleSimulation;
    class AttachToObjectWithRelease;
    class ChangePlayerSides;
  };

  class DeveloperModules {
    file = "\kauk_godmode\modules\Developer";
    class DebugConsole;
    class OpenViewerMenu;
    class GetObjectClass;
    class ListVariables;
  };

  class EquipmentModules {
    file = "\kauk_godmode\modules\Equipment";
    class EquipmentLights;
    class EquipmentNvgs;
    class EquipmentThermals;
    class RemoveOrdnance;
    class RemoveCargo;
    class RemoveInventory;
    class AddWeapon;
    class AddWeaponVehicles;
    class AddWeaponInfantry;
    class RemoveWeapon;
    class CopyLoadout;
  };

  class SaveLoadModules {
    file = "\kauk_godmode\modules\SaveLoad";
    class PasteObjectsAbsolute;
    class PasteObjectsRelative;
    class SaveObjectsForPaste;
  };

  class SpawnModules {
    file = "\kauk_godmode\modules\Spawn";
    class ForceRemoveArea;
    class ForceRemoveObject;
    class SpawnMenuAdvanced;
    class SpawnSupportModule;
  };

  class TeleportModules {
    file = "\kauk_godmode\modules\Teleport";
    class TeleportAllPlayers;
    class TeleportGroup;
    class TeleportSide;
    class TeleportSinglePlayer;
    class TeleportZeus;
    class TeleportZeusVehicle;
  };

  class UtilModules {
    file = "\kauk_godmode\modules\Util";
    class AddAllObjectsToCuratorModule;
    class RemoveObjectsFromCuratorModule;
    class UtilSetWeatherModule;
    class LaunchStrike;
  };

  class LHDModules {
    file = "\kauk_godmode\modules\LHD";
    class SpawnLHD;
    class PlaceObjectOnLHD;
  };

};

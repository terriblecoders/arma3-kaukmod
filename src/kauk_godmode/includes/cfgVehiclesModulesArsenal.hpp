class Kauk_Module_Arsenal_AddFull : Kauk_Arsenal_Module_Base {
  scopeCurator = 2;
  displayName = "Add Arsenal to Object";
  function = "Kauk_fnc_ArsenalAddFull";
};

class Kauk_Module_Arsenal_AddEmpty : Kauk_Arsenal_Module_Base {
  scopeCurator = 2;
  displayName = "Remove Arsenal from Object";
  function = "Kauk_fnc_ArsenalAddEmpty";
};
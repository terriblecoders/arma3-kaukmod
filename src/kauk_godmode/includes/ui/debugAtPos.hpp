class kauk_DebugCall_Dialog
{
  idd = 199642;
  movingEnable = true;
  onLoad = "setMousePosition [0.3, 0.3];";

  class controls
  {
    ////////////////////////////////////////////////////////
    // GUI EDITOR OUTPUT START (by mburgherr, v1.063, #Wigoza)
    ////////////////////////////////////////////////////////

    class kauk_debugui_background: IGUIBack
    {
      idc = 1800;
      moving = 1;

      x = 1 * GUI_GRID_W + GUI_GRID_X;
      y = 0.5 * GUI_GRID_H + GUI_GRID_Y;
      w = 38 * GUI_GRID_W;
      h = 11 * GUI_GRID_H;
    };
    class kauk_debugui_editbox: RscEdit
    {
      idc = 1400;
      style = 16;
      lineSpacing = 1;
      autocomplete = "scripting";

      x = 1.5 * GUI_GRID_W + GUI_GRID_X;
      y = 1 * GUI_GRID_H + GUI_GRID_Y;
      w = 37 * GUI_GRID_W;
      h = 7 * GUI_GRID_H;
    };
    class kauk_button_local: RscButton
    {
      idc = 1600;
      onButtonClick = "_o = 0 call Kauk_fnc_DebugPos;";

      text = "Local Exec"; //--- ToDo: Localize;
      x = 29.5 * GUI_GRID_W + GUI_GRID_X;
      y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
      w = 9 * GUI_GRID_W;
      h = 1.5 * GUI_GRID_H;
    };
    class kauk_button_server: RscButton
    {
      idc = 1601;
      onButtonClick = "_o = 1 call Kauk_fnc_DebugPos;";

      text = "Server Exec"; //--- ToDo: Localize;
      x = 1.5 * GUI_GRID_W + GUI_GRID_X;
      y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
      w = 9 * GUI_GRID_W;
      h = 1.5 * GUI_GRID_H;
    };
    class kauk_button_broadcast: RscButton
    {
      idc = 1602;
      onButtonClick = "_o = 2 call Kauk_fnc_DebugPos;";

      text = "Global Exec"; //--- ToDo: Localize;
      x = 15.5 * GUI_GRID_W + GUI_GRID_X;
      y = 9.5 * GUI_GRID_H + GUI_GRID_Y;
      w = 9 * GUI_GRID_W;
      h = 1.5 * GUI_GRID_H;
    };
    ////////////////////////////////////////////////////////
    // GUI EDITOR OUTPUT END
    ////////////////////////////////////////////////////////
  };
};
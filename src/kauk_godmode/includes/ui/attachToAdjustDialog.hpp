class kauk_panel_adjustObject
{
  idd = 199642;
  movingEnable = true;
  onLoad = "setMousePosition [0.3, 0.3];";
  class controls
  {
    ////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Baguette, v1.063, #Wycamy)
////////////////////////////////////////////////////////

class kauk_MovePanel_background: IGUIBack
{
  idc = 1800;
  moving = 1;

  x = -9 * GUI_GRID_W + GUI_GRID_X;
  y = -6 * GUI_GRID_H + GUI_GRID_Y;
  w = 58 * GUI_GRID_W;
  h = 8 * GUI_GRID_H;
};
class kauk_button_front: RscButton
{
  idc = 1600;
  onButtonClick = "_o = 0 call Kauk_fnc_AlignAttachment;";

  text = "Front"; //--- ToDo: Localize;
  x = -4 * GUI_GRID_W + GUI_GRID_X;
  y = -4 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
};
class kauk_button_back: RscButton
{
  idc = 1601;
  onButtonClick = "_o = 1 call Kauk_fnc_AlignAttachment;";

  text = "Back"; //--- ToDo: Localize;
  x = -4 * GUI_GRID_W + GUI_GRID_X;
  y = -1 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
};
class kauk_button_left: RscButton
{
  idc = 1602;
  onButtonClick = "_o = 2 call Kauk_fnc_AlignAttachment;";

  text = "Left"; //--- ToDo: Localize;
  x = -8.5 * GUI_GRID_W + GUI_GRID_X;
  y = -1 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
};
class kauk_button_right: RscButton
{
  idc = 1603;
  onButtonClick = "_o = 3 call Kauk_fnc_AlignAttachment;";

  text = "Right"; //--- ToDo: Localize;
  x = 0.5 * GUI_GRID_W + GUI_GRID_X;
  y = -1 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
};
class kauk_button_up: RscButton
{
  idc = 1604;
  onButtonClick = "_o = 4 call Kauk_fnc_AlignAttachment;";

  text = "Up"; //--- ToDo: Localize;
  x = -8.5 * GUI_GRID_W + GUI_GRID_X;
  y = -4 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
};
class kauk_button_down: RscButton
{
  idc = 1605;
  onButtonClick = "_o = 5 call Kauk_fnc_AlignAttachment;";

  text = "Down"; //--- ToDo: Localize;
  x = 0.5 * GUI_GRID_W + GUI_GRID_X;
  y = -4 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
};
class kauk_text_move: RscEdit
{
  idc = 1401;

  text = "0.2"; //--- ToDo: Localize;
  x = 1.5 * GUI_GRID_W + GUI_GRID_X;
  y = -5.5 * GUI_GRID_H + GUI_GRID_Y;
  w = 5.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
  tooltip = "Distance travelled by Move buttons"; //--- ToDo: Localize;
};
class kauk_label_1: RscText
{
  idc = 1000;

  text = "Move Distance amount"; //--- ToDo: Localize;
  x = -8.5 * GUI_GRID_W + GUI_GRID_X;
  y = -5.5 * GUI_GRID_H + GUI_GRID_Y;
  w = 9.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_button_simulation: RscButton
{
  idc = 1611;
  onButtonClick = "_o = 6 call Kauk_fnc_AlignAttachment;";

  text = "Toggle Simulation"; //--- ToDo: Localize;
  x = 5 * GUI_GRID_W + GUI_GRID_X;
  y = -4 * GUI_GRID_H + GUI_GRID_Y;
  w = 7.5 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
  tooltip = "Toggle Simulation of Selected Object"; //--- ToDo: Localize;
};
class kauk_button_setAngle: RscButton
{
  idc = 1608;
  onButtonClick = "_o = 7 call Kauk_fnc_AlignAttachment;";

  text = "Set Angle"; //--- ToDo: Localize;
  x = 5 * GUI_GRID_W + GUI_GRID_X;
  y = -1 * GUI_GRID_H + GUI_GRID_Y;
  w = 7.5 * GUI_GRID_W;
  h = 2.5 * GUI_GRID_H;
};
class kauk_label_4: RscText
{
  idc = 1005;

  text = "X Axis:"; //--- ToDo: Localize;
  x = 13 * GUI_GRID_W + GUI_GRID_X;
  y = -4 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_label_5: RscText
{
  idc = 1006;

  text = "Y Axis:"; //--- ToDo: Localize;
  x = 13 * GUI_GRID_W + GUI_GRID_X;
  y = -2 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_label_6: RscText
{
  idc = 1007;

  text = "Z Axis:"; //--- ToDo: Localize;
  x = 13 * GUI_GRID_W + GUI_GRID_X;
  y = 0 * GUI_GRID_H + GUI_GRID_Y;
  w = 4 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_text_xaxis: RscEdit
{
  idc = 1403;

  text = "0"; //--- ToDo: Localize;
  x = 17 * GUI_GRID_W + GUI_GRID_X;
  y = -4 * GUI_GRID_H + GUI_GRID_Y;
  w = 6.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_text_yaxis: RscEdit
{
  idc = 1404;

  text = "0"; //--- ToDo: Localize;
  x = 17 * GUI_GRID_W + GUI_GRID_X;
  y = -2 * GUI_GRID_H + GUI_GRID_Y;
  w = 6.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_text_zaxis: RscEdit
{
  idc = 1405;

  text = "0"; //--- ToDo: Localize;
  x = 17 * GUI_GRID_W + GUI_GRID_X;
  y = 0 * GUI_GRID_H + GUI_GRID_Y;
  w = 6.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_slider_alignx: RscSlider
{
  idc = 1900;
  onSliderPosChanged = "_o = [8,_this select 1] call Kauk_fnc_AlignAttachment;";

  x = 25 * GUI_GRID_W + GUI_GRID_X;
  y = -4 * GUI_GRID_H + GUI_GRID_Y;
  w = 22.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_slider_aligny: RscSlider
{
  idc = 1901;
  onSliderPosChanged = "_o = [9,_this select 1] call Kauk_fnc_AlignAttachment;";

  x = 25 * GUI_GRID_W + GUI_GRID_X;
  y = -2 * GUI_GRID_H + GUI_GRID_Y;
  w = 22.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
class kauk_slider_alignz: RscSlider
{
  idc = 1902;
  onSliderPosChanged = "_o = [10,_this select 1] call Kauk_fnc_AlignAttachment;";

  x = 25 * GUI_GRID_W + GUI_GRID_X;
  y = 0 * GUI_GRID_H + GUI_GRID_Y;
  w = 22.5 * GUI_GRID_W;
  h = 1 * GUI_GRID_H;
};
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT END
////////////////////////////////////////////////////////
  };
};
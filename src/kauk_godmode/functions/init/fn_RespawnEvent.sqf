/*
|----------------------------------------------------------------------
|Name: respawn_event.sqf
|----------------------------------------------------------------------
|Description:
|Script file to be used for the respawn event handler which adds
|back Zeus, and the Control panel.
|
|Usage:
|Tied to Respawn eventhandler of player
|
|Parameters:
|None
|----------------------------------------------------------------------
*/

call Kauk_fnc_ZeusRefresh;

//Initial Player is always the real player body. This is in respawn so the respawned body gets properly registered again.
initialPlayer = player;

//State Settings
initialPlayer setVariable ["Kauk_ModuleState", 0];           // Module status variable for modules with multiple statuses
initialPlayer setVariable ["Kauk_CP_IsInvinsible", false];   // Player is invinsible
initialPlayer setVariable ["Kauk_CP_IsGod", false];          // Player has Godmode
initialPlayer setVariable ["Kauk_CP_ReviveActivated", false];// Player has Respawn
initialPlayer setVariable ["Kauk_CP_Fatigue", true];         // Fatigue state of player
initialPlayer setVariable ["Kauk_CP_Grass", true];         // Grass state of player
initialPlayer setVariable ["Kauk_CP_LeaderMove", true];      // Is AI Leader allowed to move?


{#/*
  ACE Specific Settings

  These Will get applied to the player everytime the player respawns.
  
*/#}
{%- if general.mod_available_ace -%}
  {% if godmode.add_player_as_ace_medic -%}
    player setVariable ["ace_medical_medicClass", 1];
  {%- endif -%}
  {% if godmode.add_player_as_ace_engineer -%}
      player setVariable ["ACE_IsEngineer", 1];
  {%- endif -%}
{%- endif -%}

//Add player as Zeus Editable object
{% if godmode.add_player_to_zeusEditable -%}
  [[player]] call Kauk_fnc_AddUnitsToCurator;
{%- endif %}
{#/*
|----------------------------------------------------------------------
|Name: fn_InitAres.sqf
|----------------------------------------------------------------------
|Description:
|Custom Init script that handles personal settings and options outside
|of what Ares usually provides
|
|Usage:
|Loaded by Config.cpp
|
|Parameters:
|None
|----------------------------------------------------------------------
*/#}

// Blacklist for modules that grab objects. These types should not be added to Zeus automatically.
Kauk_EditableObjectBlacklist = [
  "Kauk_Module_Util_Create_Composition",
  "Kauk_Module_Save_Objects_For_Paste",
  "ModuleCurator_F",
  "GroundWeaponHolder",
  "Salema_F",
  "Ornate_random_F",
  "Mackerel_F",
  "Tuna_F",
  "Mullet_F",
  "CatShark_F",
  "Rabbit_F",
  "Snake_random_F",
  "Turtle_F",
  "Hen_random_F",
  "Cock_random_F",
  "Cock_white_F",
  "Sheep_random_F"
];

//Remove All Curators to avoid their limitations to affect the player
//TODO: Create Userconfig to allow this to be turned off.
{ deleteVehicle _x; } count allCurators;

Kauk_custom_logic = createGroup sideLogic;
Kauk_custom_curator = (Kauk_custom_logic) createunit ["ModuleCurator_F", [0, 90, 90],[],0.5,"NONE"];
Kauk_custom_curator setvariable ['Addons',3,true];

module_attachment_release_list = []; // [Object, action_id, id]
module_attachment_release_index = 0;

option_CfgWeapon = "";
STATE_AddWeaponVehiclesCache = true;
STATE_AddWeaponInfantryCache = true;

//Zeus Related Quick Action functions
fn_CuratorKeyPressed  = {switch (_this select 1) do {case 29:{Kauk_Ctrl_Key_Pressed = true;};};false;};
fn_CuratorKeyReleased = {switch (_this select 1) do {case 29:{Kauk_Ctrl_Key_Pressed = false;};};false;};
fn_CuratorDoubleClick = {
  _clickedObject = _this select 1;
  _handled = false;
  if (not isNil "Kauk_Ctrl_Key_Pressed") then {
    if (Kauk_Ctrl_Key_Pressed) then {
      _handled = true;
      [_clickedObject] spawn {
        _clickedObject = _this select 0;
        closeDialog 1;
        sleep 0.05;
        kauk_curatorPropertyObject = _clickedObject;
        createDialog "kauk_curatorPropertyDialog";
      };
    };
  };
  _handled;
};

//Detect if certain Third Party Addons are available
if (isClass(configFile>>"CfgPatches">>"ace_main")) then {KAUK_ADDON_ACE = true;} else {KAUK_ADDON_ACE = false;};
if (isClass(configFile>>"CfgPatches">>"task_force_radio"))         then {KAUK_ADDON_TFAR = true;} else {KAUK_ADDON_TFAR = false;};

[] spawn {
  //Lets keep things working even when you respawn
  waitUntil {!isNull player};
  [] call Kauk_fnc_RespawnEvent;
  player addEventhandler ["Respawn", {[] call Kauk_fnc_RespawnEvent;}];

  // Wait until player is a zeus unit
  while { !([player] call Kauk_fnc_isZeus) } do { sleep 1; };

  ["Kauk"] spawn Kauk_fnc_MonitorCuratorDisplay;
  {
    if ((getassignedcuratorunit _x) == player) then {
      curatorObjectDoubleClickedEH = _x addEventHandler ["CuratorObjectDoubleClicked", { _this call fn_CuratorDoubleClick; }];
      curatorObjectPlacedEH = _x addEventHandler ["CuratorObjectPlaced", { _this call Kauk_fnc_HandleCuratorObjectPlaced; }];
    };
  } count allCurators;
};

{% if godmode.enable_admin_panel -%}
  [] spawn {
    sleep 5;
    ["KaukMod", "kauk_admincp", "Open Admin Panel", {createDialog "kauk_controlpanel_admin";}, {false}, [38, [true, false, false]],false] call CBA_fnc_addKeybind;
  };
{% endif %}
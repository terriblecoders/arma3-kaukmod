/*
 * Author: Bohemia Interactive
 * Module function for remote controlling units as zeus
 * Edited to remove global wind sound
 *
 * Arguments:
 * 0: The module logic <LOGIC>
 * 1: units <ARRAY>
 * 2: activated <BOOL>
 *
 * Return Value:
 * nil
 *
 * Public: No
 */
[_this select 0] spawn {
      //--- Terminate when remote control is already in progress
      if !(isnull (missionnamespace getvariable ["bis_fnc_moduleRemoteControl_unit",objnull])) exitwith {};

      _unit = _this select 0;
      if (true) then {
        _vehicle = vehicle _unit;

        _mainSeatArray = [
          format ["Driver: %1", driver _vehicle],
          format ["Gunner: %1", gunner _vehicle],
          format ["Commander: %1", commander _vehicle]
        ];

        _dialogResult = [
          "Remote control Slot",
          [
            ["Position", _mainSeatArray]
          ]
        ] call Kauk_fnc_ShowChooseDialog;

        switch (_dialogResult select 0) do {
          case 0: { _unit = driver _vehicle; };     //Driver
          case 1: { _unit = gunner _vehicle; };     //Gunner
          case 2: { _unit = commander _vehicle; };  //Commander
          default { _unit = driver _vehicle; };
        };
      };

      //--- Check if the unit is suitable
      _error = "";
      if !(side group _unit in [east,west,resistance,civilian]) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorEmpty";};
      if (isplayer _unit) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorPlayer";};
      if !(alive _unit) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorDestroyed";};
      if (isnull _unit) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorNull";};
      if !(isnull (_unit getvariable ["bis_fnc_moduleRemoteControl_owner",objnull])) then {_error = localize "str_a3_cfgvehicles_moduleremotecontrol_f_errorControl";};

      if (_error == "") then {
          _unit spawn {
              scriptname "bis_fnc_moduleRemoteControl: Loop";
              _unit = _this;
              _vehicle = vehicle _unit;
              _vehicleRole = str assignedvehiclerole _unit;

              bis_fnc_moduleRemoteControl_unit = _unit;
              _unit setvariable ["bis_fnc_moduleRemoteControl_owner",player,true];

              //--- Wait for interface to close
              (finddisplay 312) closedisplay 2;
              waituntil {isnull curatorcamera};

              //--- remote control unit
              player remotecontrol _unit;
              if (cameraon != _vehicle) then {
                  _vehicle switchcamera cameraview;
              };

              _curator = getassignedcuratorlogic player;
              [_curator,"curatorObjectRemoteControlled",[_curator,player,_unit,true]] call bis_fnc_callScriptedEventHandler;
              [["Curator","RemoteControl"],nil,nil,nil,nil,nil,nil,true] call bis_fnc_advHint;

              //--- Back to player
              _vehicle = vehicle _unit;
              _vehicleRole = str assignedvehiclerole _unit;
              _rating = rating player;
              waituntil {
                  //--- Refresh when vehicle or vehicle role changes
                  if ((vehicle _unit != _vehicle || str assignedvehiclerole _unit != _vehicleRole) && {alive _unit}) then {
                      player remotecontrol _unit;
                      _vehicle = vehicle _unit;
                      _vehicleRole = str assignedvehiclerole _unit;
                  };
                  if (rating player < _rating) then {
                      player addrating (-rating player + _rating);
                  };
                  sleep 0.01;
                  !isnull curatorcamera
                  ||
                  {cameraon == vehicle player}
                  ||
                  {!alive _unit} //--- Also isnull check, objNull is not alive
                  ||
                  {isnull getassignedcuratorlogic player}
                  //||
                  //{_unit getvariable ["bis_fnc_moduleRemoteControl_owner",objnull] != player} //--- Another curator stole the unit
              };

              player addrating (-rating player + _rating);
              objnull remotecontrol _unit;
              _unit setvariable ["bis_fnc_moduleRemoteControl_owner",nil,true];

              //--- Death screen
              if (
                  isnull curatorcamera
                  &&
                  {cameraon != vehicle player}
                  &&
                  {!isnull _unit}
                  &&
                  {!isnull getassignedcuratorlogic player}
                  //&&
                  //{(_unit getvariable ["bis_fnc_moduleRemoteControl_owner",objnull] == player)}
              ) then {
                  sleep 2;
                  ("bis_fnc_moduleRemoteCurator" call bis_fnc_rscLayer) cuttext ["","black out",1];
                  sleep 1;
              };
              _unitPos = getposatl _unit;
              _camPos = [_unitPos,10,direction _unit + 180] call bis_fnc_relpos;
              _camPos set [2,(_unitPos select 2) + (getterrainheightasl _unitPos) - (getterrainheightasl _camPos) + 10];
              //[_camPos,_unit] call bis_fnc_setcuratorcamera;
              (getassignedcuratorlogic player) setvariable ["bis_fnc_modulecuratorsetcamera_params",[_camPos,_unit]];

              sleep 0.1; //--- Engine needs a delay in case controlled unit was deleted
              ("bis_fnc_moduleRemoteCurator" call bis_fnc_rscLayer) cuttext ["","black in",1e10];
              opencuratorinterface;

              waituntil {!isnull curatorcamera};
              player switchcamera cameraview;
              bis_fnc_moduleRemoteControl_unit = nil;
              ("bis_fnc_moduleRemoteCurator" call bis_fnc_rscLayer) cuttext ["","black in",1];
              [_curator,"curatorObjectRemoteControlled",[_curator,player,_unit,false]] call bis_fnc_callScriptedEventHandler;
              sleep 0.01;
          };
      } else {
          [objnull,_error] call bis_fnc_showCuratorFeedbackMessage;
      };
};
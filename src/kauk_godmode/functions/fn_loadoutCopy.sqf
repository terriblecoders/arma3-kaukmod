//Get Uniform, Vest and Backpack
_uniform = uniform _this;
_vest = vest _this;
_backpack = backpack _this;

//Get Uniform, Vest and Backpack Items
_uniform_items = uniformItems _this;
_vest_items = vestItems _this;
_backpack_items = backpackItems _this;

//Get Linked items and weapons
_assigned_items = assignedItems _this;
_weapons = weapons _this;
_acc_primary = primaryWeaponItems _this;
_acc_secondary = secondaryWeaponItems _this;
_acc_handgun = handgunItems _this;

//Get Goggles and Headgear
_headgear = headgear _this;
_goggles = goggles _this;

_units = units group _this;
{
  if (!(_x == _this)) then {
    _unit = _x;

    //Remove every Item the unit
    removeAllWeapons _unit;
    removeAllItems _unit;
    removeAllAssignedItems _unit;
    removeUniform _unit;
    removeVest _unit;
    removeBackpack _unit;
    removeHeadgear _unit;
    removeGoggles _unit;

    _unit forceAddUniform _uniform;
    _unit addVest _vest;
    _unit addBackpack _backpack;

    {_unit addItemToUniform _x;} count _uniform_items;
    {_unit addItemToVest _x;} count _vest_items;
    {_unit addItemToBackpack _x;} count _backpack_items;
    {_unit addWeapon _x;} count _weapons;
    {_unit linkItem _x;} count _assigned_items;

    _unit addHeadgear _headgear;
    _unit addGoggles _goggles;

    //Add Weapon Attachments
    {_unit addPrimaryWeaponItem _x;} count _acc_primary;
    {_unit addSecondaryWeaponItem _x;} count _acc_secondary;
    {_unit addHandgunItem _x;} forEach _acc_handgun;

  };
} count _units;



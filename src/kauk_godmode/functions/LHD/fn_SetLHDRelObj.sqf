{#/*
|----------------------------------------------------------------------
|Name: fn_SetLHDRelObj.sqf
|----------------------------------------------------------------------
|Description:
|Sets up the LHD's Decorations Objects and Special Functionality.
|
|
|Usage:
|[Object, RelativePos, Direction] call Kauk_fnc_SetLHDRelObj;
|
|Parameters:
|Object - Object to Move
|RelativePos - Relative Position of the Object with ASL Z Axis
|Direction - In Which Direction the LHD should be facing
|----------------------------------------------------------------------
*/#}

_object = (_this select 0);
_position = (_this select 1);
_direction = (_this select 2);

//Get Height of the LHD Itself
_height = (getPosASL KAUKMOD_LHD) select 2;

_pos_oject = (KAUKMOD_LHD modelToWorld _position);
_object setPosASL [_pos_oject select 0, _pos_oject select 1, (_height + (_position select 2))];
_object setDir ((direction KAUKMOD_LHD) + _direction);

{#/*
|----------------------------------------------------------------------
|Name: fn_SetupLHD.sqf
|----------------------------------------------------------------------
|Description:
|Sets up the LHD's Decorations Objects and Special Functionality.
|
|
|Usage:
|[PositionASL, Direction] call Kauk_fnc_SetupLHD;
|
|Parameters:
|PositionASL - Position of the LHD
|Direction - In Which Direction the LHD should be facing
|----------------------------------------------------------------------
*/#}

KAUKMOD_LHD = "ATLAS_B_LHD_helper" createVehicle (_this select 0);
KAUKMOD_LHD setDir (_this select 1);
KAUKMOD_LHD setPosASL [getPos KAUKMOD_LHD select 0, getPos KAUKMOD_LHD select 1, 0];

[] spawn {
  sleep 5;

  {%- if godmode.lhd.place_lamps -%} 
    _light_poles = [[-1.95996,41.7715,17],[-6.73633,-26.2441,17],[-0.174805,121.507,14]];
    {_object = "Land_LampAirport_F" createVehicle [0,0,0]; _object allowDamage false; [_object, _x, 0] call Kauk_fnc_SetLHDRelObj;} count _light_poles;
  {%- endif -%} 

  {%- if godmode.lhd.place_bunkers -%} 
    _object1 = "Land_BagBunker_Small_F" createVehicle [0,0,0];
    _object2 = "Land_BagBunker_Small_F" createVehicle [0,0,0];
    _object1 allowDamage false; [_object1, [-10.5, 115.5, 16.65], 90] call Kauk_fnc_SetLHDRelObj; //Back Right
    _object2 allowDamage false; [_object2, [10.5, 115.5, 16.65], 270] call Kauk_fnc_SetLHDRelObj; //Back Left
  {%- endif -%} 

  {%- if godmode.lhd.place_aa -%} 
    _aa1 = "kauk_fp_mim23" createVehicle [0,0,0];
    _aa1 allowDamage false; [_aa1, [-12, -32.5, 20.5], 180] call Kauk_fnc_SetLHDRelObj;
  {%- endif -%} 

  {%- if godmode.lhd.place_flags -%} 
    _flag1 = "Flag_POWMIA_F" createVehicle [0,0,0];
    _flag1 allowDamage false; [_flag1, [-3, 15, 25.5], 0] call Kauk_fnc_SetLHDRelObj;
  {%- endif -%} 
};

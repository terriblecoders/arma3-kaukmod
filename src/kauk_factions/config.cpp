class CfgPatches {
  class kauk_factions {
    units[] = {
      {% if factions.enable_french_army %}
      "Kauk_FR_Rifleman"
      {% endif %}
    };
    weapons[] = {};
    requiredVersion = {{ general.required_version }};
    requiredAddons[] = {};
    author[] = {"{{ general.author }}"};
    authorUrl = "{{ general.author_url }}";
    version = {{ general.version }};
  };
};

class CfgFactionClasses {
    {#/*
      Keep in mind that the units inside this faction class most likely won't be compatible with Alive, since they don't follow the usual VehicleClasses config.
      This is due to the fact that I want to have all "Factions" inside my own little place for easy and ordered access.
    */#}
    class kauk_faction_units {
      displayName = "KaukMod Custom Factions";
      priority = 1;
      side = 1;
      flag = "\a3\Data_f\Flags\flag_nato_co.paa";
  		icon = "\a3\Data_f\cfgFactionClasses_BLU_ca.paa";
    };
};

class CfgVehicleClasses {
  {% if factions.enable_french_army %}
    class Kauk_FR_Men {
      displayName = "French Armed Forces";
    };
  {% endif %}
};

class CfgVehicles {
  class I_Soldier_SL_F { class Eventhandlers; };
  class I_Soldier_TL_F { class Eventhandlers; };
  class I_medic_F { class Eventhandlers; };
  class I_Soldier_AR_F { class Eventhandlers; };
  class I_Soldier_LAT_F { class Eventhandlers; };
  class I_Soldier_GL_F { class Eventhandlers; };
  class I_Soldier_AT_F { class Eventhandlers; };
  class I_Soldier_F { class Eventhandlers; };
  class I_Sniper_F { class Eventhandlers; };
  class I_Soldier_A_F { class Eventhandlers; };
  class I_crew_F { class Eventhandlers; };
  class I_officer_F { class Eventhandlers; };
  class I_helipilot_F { class Eventhandlers; };
  class I_engineer_F { class Eventhandlers; };
  class C_man_1 { class Eventhandlers; };
  class Civilian_F { class Eventhandlers; };

  {% if factions.enable_french_army %}{% include "./includes/men/french.hpp" %}{% endif %}
};
{% set i_linked_items = '
  "V_Chestrig_oli",
  "rhs_Booniehat_m81",
  "ItemMap",
  "ItemCompass",
  "ItemWatch",
  "ItemRadio"
'%}

{% set i_magazines = '
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "R3F_25Rnd_556x45_FAMAS",
  "RH_17Rnd_9x19_g17",
  "RH_17Rnd_9x19_g17",
  "RH_17Rnd_9x19_g17",
  "HandGrenade",
  "HandGrenade",
  "SmokeShell",
  "SmokeShellGreen",
  "Chemlight_green",
  "Chemlight_green"
'%}

{% set i_weapons = '
  "R3F_Famas_F1_NR",
  "RH_g19",
  "Throw",
  "Put"
'%}

  class Kauk_FR_Rifleman : I_Soldier_F {
    _generalMacro = "Kauk_FR_Rifleman";
    author = "{{ general.author }}";

    scope = 2;
    side = 1;

    faction = "kauk_faction_units";
    vehicleClass = "Kauk_FR_Men";

    displayName = "Rifleman";

    identityTypes[]=
    {
      "LanguagePER_F",
      "Head_TK",
      "G_IRAN_default"
    };

    faceType = "Man_A3";
    genericNames="TakistaniMen";
    uniformClass = "rhs_uniform_FROG01_m81";
    
    linkedItems[] = { {% autoescape false %}{{ i_linked_items }}{% endautoescape %} };
    respawnLinkedItems[] = { {% autoescape false %}{{ i_linked_items }}{% endautoescape %} };
    magazines[] = { {% autoescape false %}{{ i_magazines }}{% endautoescape %} };
    respawnMagazines[] = { {% autoescape false %}{{ i_magazines }}{% endautoescape %} };
    weapons[] = { {% autoescape false %}{{ i_weapons }}{% endautoescape %} };
    respawnWeapons[] = { {% autoescape false %}{{ i_weapons }}{% endautoescape %} };
  };
class CfgPatches
{
  class kauk_vehicle_patches
  {
    units[] = {};
    weapons[] = {};
    author[] = {"{{ general.author }}"};
    authorUrl = "{{ general.author_url }}";
    requiredVersion = {{ general.required_version }};
    requiredAddons[] = {};
    version = {{ general.version }};
  };
};

class CfgVehicles {
  class B_Heli_Light_01_armed_F {     //Armed Littlebird
    magazines[] = {                   //Add Flares Magazines to the Littlebird
      "5000Rnd_762x51_Belt",
      "24Rnd_missiles",
      "168Rnd_CMFlare_Chaff_Magazine"
    };
    weapons[] = {                     //Add Flare Weapon to the Littlebird
      "M134_minigun",
      "missiles_DAR",
      "CMFlareLauncher"
    };
    lockDetectionSystem = 12;         //Add Lock Detection to Littlebird
    incomingMissileDetectionSystem = 16;  //Add Incoming missile Warning to Littlebird
  };
  class B_Heli_Light_01_F { // Unarmed Littlebird
    {% include "./includes/unarmed_countermeasures.hpp" %}
  };
  class C_Heli_Light_01_civil_F { // Civillian Littlebird
    {% include "./includes/unarmed_countermeasures.hpp" %}
  };
};
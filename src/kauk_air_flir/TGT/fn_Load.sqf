[] spawn {
  sleep 5;
  ["KaukMod", "kauk_flittgt", "Aircraft Mark Target", {[] call kauk_fnc_laserTarget}, {false}, [44, [true, false, false]],false] call CBA_fnc_addKeybind;

  player setVariable ["Kauk_TGT_On", false];
  kauk_TGT_laserObj = objNull;

  kauk_fnc_laserTarget = {
    if (vehicle player isKindOf "Air") then {
      _state = player getVariable "Kauk_TGT_On";
      if (_state) then {
        deleteVehicle kauk_TGT_laserObj;
        kauk_TGT_laserObj = objNull;

        hint "TGT OFF";
        player setVariable ["Kauk_TGT_On", false];
      } else {

        _ins = lineIntersectsSurfaces [
          AGLToASL positionCameraToWorld [0,0,0],
          AGLToASL positionCameraToWorld [0,0,3000],
          player,
          objNull,
          true,
          1,
          "GEOM",
          "NONE"
        ];

        kauk_TGT_laserObj = "Kauk_LaserDesignation" createVehicle [0,0,0];
        kauk_TGT_laserObj setPosASL (_ins select 0 select 0);

        hint "TGT ON";
        player setVariable ["Kauk_TGT_On", true];
      };
    };
  };
};
class USAF_F16 : Plane {
  memoryPointDriverOptics[] = {"Wheel_1"};
  {% include "./optics.hpp" %}
};

class usaf_f22 : Plane {
  memoryPointDriverOptics[] = {"levy prach"};
  {% include "./optics.hpp" %}
};

class USAF_F35A : Plane_Base_F {
  memoryPointDriverOptics[] = {"WheelDust_left_pos"};
  {% include "./optics.hpp" %}
};

class USAF_A10 : Plane {
  memoryPointDriverOptics[] = {"levy prach"};
  {% include "./optics.hpp" %}
};
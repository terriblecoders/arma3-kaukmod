//
// NATO Vehicles
//

class B_Plane_CAS_01_F {  // NATO A10
  memoryPointDriverOptics[] = {"levy WheelDust_left_pos"};
  {% include "./optics.hpp" %}
};

class B_Heli_Light_01_armed_F { //NATO Armed Littlebird
  {% include "./optics.hpp" %}
};

class B_Heli_Light_01_F { // NATO Unarmed Littlebird
  {% include "./optics.hpp" %}
};

//
// CSAT Vehicles
//

class O_Plane_CAS_02_F { // CSAT Fighter Jet
  memoryPointDriverOptics[] = {"WheelDust_left_pos"};
  {% include "./optics.hpp" %}
};

class O_Heli_Light_02_F { // CSAT Kasatka (Armed)
  {% include "./optics.hpp" %}
};

class O_Heli_Light_02_unarmed_F { // CSAT Kasatka (Unarmed)
  {% include "./optics.hpp" %}
};

class O_Heli_Light_02_v2_F { // CSAT Kasatka (Orca)
  {% include "./optics.hpp" %}
};

//
// AAF Vehicles
//

class I_Plane_Fighter_03_AA_F { // AAF AA Plane
  memoryPointDriverOptics[] = {"levy prach"};
  {% include "./optics.hpp" %}
};

class I_Plane_Fighter_03_CAS_F { // AAF CAS Plane
  memoryPointDriverOptics[] = {"levy prach"};
  {% include "./optics.hpp" %}
};

class I_Heli_light_03_F { // AAF Wildcat (Armed)
  {% include "./optics.hpp" %}
};

class I_Heli_light_03_unarmed_F { // AAF Wildcat (Unarmed)
  {% include "./optics.hpp" %}
};

//
// Civillian Vehicles
//

class C_Heli_Light_01_civil_F { // Civillian Littlebird
  {% include "./optics.hpp" %}
};
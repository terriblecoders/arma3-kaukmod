class pilotCamera {
  class OpticsIn {
    class Wide {
      opticsDisplayName = "W";
      initAngleX = 0;
      minAngleX = 0;
      maxAngleX = "+0";
      initAngleY = 0;
      minAngleY = 0;
      maxAngleY = "+0";
      initFov = 0.5;
      minFov = 0.5;
      maxFov = 0.5;
      visionMode[] = {"Normal", NVG, "TI"};
      thermalMode[] = {0, 1};

      //0, 1 == Black White
      //2, 3 == Green Black
      //5, 6 == Orange Black

      gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Heli_Attack_01_Optics_Gunner_wide_F";
    };

    class Medium : Wide {
      opticsDisplayName = "M";
      initFov = 0.1;
      minFov = 0.1;
      maxFov = 0.1;
      gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Heli_Attack_01_Optics_Gunner_medium_F";
    };

    class Narrow : Wide {
      opticsDisplayName = "N";
      initFov = 0.0286;
      minFov = 0.0286;
      maxFov = 0.0286;
      gunnerOpticsModel = "\A3\Weapons_F_Beta\Reticle\Heli_Attack_01_Optics_Gunner_narrow_F";
    };
    showMiniMapInOptics = 1;
    showUAVViewpInOptics = 0;
    showSlingLoadManagerInOptics = 0;
  };
  minTurn = -360;
  maxTurn = 360;
  initTurn = 0;
  minElev = -80;
  maxElev = 80;
  initElev = 0;
  maxXRotSpeed = 0.4;
  maxYRotSpeed = 0.4;
  pilotOpticsShowCursor = 1;
  controllable = 1;
};
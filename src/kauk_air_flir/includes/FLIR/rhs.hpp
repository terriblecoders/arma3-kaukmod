//
// US Army Vehicles
//

class RHS_AH64_base : Heli_Attack_01_base_F {
  memoryPointDriverOptics[] = {"Rocket_1"};
  {% include "./optics.hpp" %}
};

class RHS_AH1Z_base : Heli_Attack_01_base_F {
  memoryPointDriverOptics[] = {"Rocket_1"};
  {% include "./optics.hpp" %}
};

class RHS_A10 : Plane_CAS_01_base_F {
  memoryPointDriverOptics[] = {"levy prach"};
  {% include "./optics.hpp" %}
};

//
// Russian Vehicles
//
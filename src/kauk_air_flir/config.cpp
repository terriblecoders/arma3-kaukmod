class CfgPatches
{
  class kauk_air_flir
  {
    units[] = {"Kauk_LaserDesignation", "Kauk_LaserDesignation_opf"};
    weapons[] = {};
    author[] = {"{{ general.author }}"};
    authorUrl = "{{ general.author_url }}";
    requiredVersion = {{ general.required_version }};
    requiredAddons[] = {};
    version = {{ general.version }};
  };
};



class CfgVehicles {

  class Plane;
  class Helicopter;
  class Plane_Base_F : Plane {};
  class Helicopter_Base_F : Helicopter {};
  class Plane_CAS_01_base_F : Plane_Base_F {};
  class Heli_Attack_01_base_F : Helicopter_Base_F {};

  {% include "./includes/FLIR/vanilla.hpp" %}
  {% include "./includes/FLIR/rhs.hpp" %}
  {% include "./includes/FLIR/usaf.hpp" %}

  {% if air_flir.enable_tgt %}
    class Sign_Sphere25cm_F;
    class Kauk_LaserDesignation : Sign_Sphere25cm_F {
      displayName = "Custom Laser Target";
      author = "{{ general.author }}";
      nameSound = "";
      threat[] = {0.0, 0.0, 1.0};
      camouflage = 99;
      accuracy = 0;
      alwaysTarget = 1;
      type = "VArmor";
      simulation = "thing";
      laserTarget = 1;
      irTarget = 0;
      nvTarget = 0;
      artilleryTarget = 0;
      allowTabLock = 1;
      scope = 1;
      side = 0;
    };
  {% endif %}
};

{% if air_flir.enable_tgt %}
  class CfgFunctions {
    class kauktgt {
      class tgt {
        file = "\kauk_air_flir\TGT";
        class Load { preInit = 1; };
      };
    };
  };
{% endif %}
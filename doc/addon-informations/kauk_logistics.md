#Logistics
This addon provides Logistic functionalities that work across all missions in Singleplayer, Multiplayer and in the Editor.

Functionalities include the following:

* Slingloading Simple (using attachTo, Same way R3F Logistic does it)
* Slingloading Modern (using Ropes and PhysX. Works Similiar to Vanilla Slingloading but works with every object)
* Loading into vehicles
* Dragging Objects

#Requirements
* None

#License
This project is licensed under the MIT License which can be found in the licenses folder.
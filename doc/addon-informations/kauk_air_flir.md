#Air FLIR
This addon equips a variety of Aircraft and Helicoper with FLIR Cameras with NV and TI capabilities.

The FLIR Camera is provided by the Slingloading Camera and uses values and Optics from the Vanilla MQ4A UAV Drone.

#Requirements
* RHS
* USAF

Each Addon Requirement can be manually removed by removing the include to the Mod you don't want to use.

For example removing
```
#include "FLIR\rhs.hpp"
```
would remove the dependency to RHS.

#License
This project is licensed under the MIT License which can be found in the licenses folder.
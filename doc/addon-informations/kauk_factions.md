#Factions

This Addon provides custom factions that are used within the FPArma modpack and missions.
As of right now, this addon is only a proof-of-concept and in the future will be filled with
various factions.

#Requirements

* HLC Weapons
* RHS
* FParma Uniforms
* R3F Famas

Some others I forgot. Prone to change once I touch this again

#License
This project is licensed under the MIT License which can be found in the licenses folder.
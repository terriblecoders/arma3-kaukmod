#vehicle Patches

This addon provides a variety of small changes to the vehicles I use.

As of right now, the patches include the following:
* Littlebird Flare Addition

#Requirements
* None

#License
This project is licensed under the MIT License which can be found in the licenses folder.
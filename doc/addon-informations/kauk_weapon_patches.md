#Weapon Patches

This addon provides a variety of small changes to the weapons I use.

As of right now, the patches include the following:
* HuntIR Compatibility for Community Rifles.
* R3F Riflegrenades Compatible with ASDG Muzzles.

#Requirements

## HuntIR Config Dependencies
* CUP Weapon Pack
* RHS: Escalation
* RH Weapon Pack
* R3F Famas

## R3F Riflegrenades Dependencies
* ASDG Jointrails
ASDG Jointrails will be merged with CBA in the near future, so this dependency will be replaced with CUP when this happens. (Source: https://github.com/acemod/ACE3/pull/2176)

#License
This project is licensed under the MIT License which can be found in the licenses folder.
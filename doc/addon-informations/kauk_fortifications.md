#Fortifications
This addon Provides CfgPatches entries for a selected amount of Arma 1 and Arma 2 objects provided by the AllInArma Terrain Pack.
This allows these Objects to be spawned trough Zeus, and allows them to be moved around by Zeus.

#Requirements
* AllInArma Terrain Pack / CUP Terrain Pack

#License
This project is licensed under the MIT License which can be found in the licenses folder.
In order to build this addon / compliation of addons, you need the following Software / Tools:

* NodeJS [Link](https://nodejs.org/en/)
* A Bash shell with Core Unix Tools

For the Bash shell on Windows I recommend either MSYS2 or Cmder with msysgit support.

* [MSYS2](https://msys2.github.io/)
* [Cmder](http://cmder.net/)

After you have installed these tools, make sure that the npm and node commands of the NodeJS Runtime are in your path.

Now open the terminal software of your choice inside the project folder, and issue the following commands:

```
npm -g install gulp
npm install
./make.sh
```

After that, the addon folders are ready inside the dist/deployment folder, and just need to be turned into a PBO file.
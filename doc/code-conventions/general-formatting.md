# Indentation

* 2 Spaces for Indentation

```
class Test: Parent {
..Class SmallTest {
....foo = "bar";
..};
}
```

It's better to use spaces for everything, instead of using Tabs for indentation, while using Spaces for seperating () [] and in function calls.

# Braces

* Opening brace on the same line as keyword
* Closing brace in own line, same level of indentation as keyword
* Only Exception are Arma SQF Loop structures.

## Yes

```
class Test: Parent {
  Class SmallTest {
    foo = "bar";
  };
}
```

## No

```
class Test: Parent
{
  Class SmallTest
  {
    foo = "bar";
  };
}
```

# Line Endings

* Line endings will always be UNIX Line Endings.
UNIX Line endings are supported on all major Text Editors apart from notepad.exe (Don't use that).

The reasoning for this is that the CR (Carriage Return) has it's roots in typewriters but is not relevant in the modern computing days anymore, with the exception of badly coded Software and Legacy Software.

Git Automatically Converts CRLF to LF Upon Committing, but there's also the formatting tool in src-tools, which uses dos2unix to convert Line-Endings as well as Tabs into Spaces.
